---
title: About
type: about
---

![vinay](/images/vinay.png)
{{< callout  >}}
Hello, I'm Vinay a FOSS enthusiast and a sysadmin.
I go by the handle `vinay-keshava` across the web.
I'm a Debian Maintainer,maintaining & working on packages of [GitLab](http://tracker.debian.org/gitlab) and its components on [Debian](https://debian.org).

{{< /callout >}}
{{< callout type="info" >}}
Email/XMPP: vinaykeshava [AT] disroot.org | Mastodon: @vinaykeshava@mastodon.world | Matrix: @vinay-keshava:mozilla.org
{{< /callout >}}


