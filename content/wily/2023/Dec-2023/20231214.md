---
title: 2023-12-14
type: docs
next: /wily/20231215/
---

- virt-customize 
	- Customizing debian generic cloud images using virt-customize
	```bash
	$ virt-customize -a debian-12-generic-amd64-20231210-1591.qcow2 \
		--root-password password:debian      

	$ virt-customize -a debian-12-generic-amd64-20231210-1591.qcow2 \
		--append-line '/etc/motd: Welcome to DeepRoot GNU/Linux'

	$ virt-customize -v -a debian-12-generic-amd64-20231210-1591.qcow2 --install vim

	$ virt-customize -v -a debian-12-generic-amd64-20231210-1591.qcow2 \
		--upload path-to-localfile:destination-file-path
	```
