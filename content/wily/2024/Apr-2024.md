---
title: 04, April 2024
type: docs
sidebar:
  open: false
---

### 2024-04-01


- Reactive Programming: Reactive programming is a programming paradigm that promotes an asynchronous, non-blocking, event driven approach to data processing. Let us understand the difference between blocking and non blocking request processing.

- Blocking and Non blocking nature of API's

- Backpressure: Controlling the rate limit of events being published to the subscriber from the publisher side, without crashing the system.

#### Spring Boot with Webflux

- `spring-boot-starter-data-r2dbc`: need of r2dbc due to blocking nature of jdbc, JDBC cannot be used in reactive programming, hence reactive relational database connectivity arised, which helps to connect relational databases to webflux.

- `jasync-r2dbc-mysql` is a dependency for reactive support for MySQL, is an async, netty based JVM database driver.

- `schema.sql`: within the classpath,which should be located in `/src/main/resources` because r2dbc doesn't support hibernate,i.e the automatic creation of tables within the database

- Repository layer: extending the `R2dbcRepository` which in turn extends the `ReactiveCrudRepository`.

- Difference between `Mono` and `Flux`.

#### Testing API's with curl

- `POST` operation:

```bash
$ curl -d '{
    "tutorialId": 5,
    "title": "breathe_become_air",
    "description": "kalanithi"
}' -H 'Content-Type: application/json' http://localhost:8080/tutorials
```

- `GET` operation:

```bash
 curl -v http://localhost:8080/tutorials/5

 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /tutorials/5 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.87.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Content-Type: application/json
< Content-Length: 89
<
{ [89 bytes data]
100    89  100    89    0     0   6098      0 --:--:-- --:--:-- --:--:--  6357{"tutorialId":5,"title":"breathe_become_air","description":"kalanithi","published":false}
* Connection #0 to host localhost left intact

```
which fetches the details.


#### Debian


- `debNuX` is the changelog version format for stable where '+' or '~' is appended depending on the package
'N' is the major version of Debian, 'X'  is a number, starting at 1, that is increased for each stable upload of this package.

- For stable updates that use the same upstream version debian revision component will end in +debNuX

- For stable updates that use the new upstream version debian revision component will end in  ~debNuX

- "Urgency" filed in the changelog is choosed based on the upload which fixes the RC bug,usual upload, or critical bugs.
urgency=low: Usual upload, with britney transition from unstable to testing in 10days
urgency=medium: RC Bug fixes, with britney transition in 5days
urgency=high: fixes security bugs and 2 days for transition.


### 2024-04-02

- Reactive Programming( Mono & Flux usage in Spring Boot): Till now we were using Mono which fetches at most zero or one object, what if in a situation we want to fetch more than one object, maybe a list of objects,here comes FLUX.

Till now we were using Mono which fetches at most zero or one object, what if in a situation we want to fetch more than one object, maybe a list of objects,here comes FLUX.

Mono is a type that represents a stream of data that emits zero or one item and few features of it are: Laziness, Error Handling and combining operators.

Flux is a type that represents a stream of data that emits zero or more items unlike mono, features of flux are: Backpressure, hot and cold publishing, error handling.

- Flux implementation in spring Boot and testing with `curl`

- Kubernetes architecture and [kubernetes comic](https://cloud.google.com/kubernetes-engine/kubernetes-comic/) providing an overview of the container technology.

### 2024-04-03

#### Kubernetes

Transition from Traditional Deployment Era -> Virtualized Deployment Era -> Container Deployment Era.

In Traditional deployment uses of physical server stack for deployment application, later use of virtual machines for running each microservice in different VM's and transitioned to Container deployment where minimal requirements to bootup the OS within an isolated environment.

Kubernetes provides the following features:

-Service discovery and load balancing
-Storage orchestration
-Automated rolling updates and rollbacks
-Automatic bin packaging
-Self healing
-Secret and configuration management
-Batch execution
-Horizontal scaling
-IPV4 / IPV6 dual stack

Kubernetes architecture

Nodes: nodes are the worker nodes, which is physical server or VM's. Pods are deployed on the node and managed by the control plane.
Pods: basic unit of deployment in Kubernetes, a pod is a group of one or more containers that are deployed on the same node. Pods host the application

Date/Node Components: Node components run on every node for maintainance of running pods.

- kubelet
- container runtime
- kubeproxy

Control Plane Components:

- kube scheduler
- etcd
- kube API server
- kube controller manager
- cloud controller manager%

### 2024-04-04

- `virt-install`: Launching a VM with the help of existing `qcow2` image.

```bash
virt-install --name <VM-NAME> --memory 2048 --vcpus 2 --disk /home/vinay/Downloads/debian-12-generic-amd64-20240211-1654.qcow2,bus=sata --import --os-variant debian12 --network default
```

Launching the vm with an existing disk image, here launching a debian 12 generic cloud image with 2GB of ram, 2 CPU's, with network as default also mentioning the os-variant `debian`.

To list all the os-variant the option is `virt-install --os-list info`.%


### 2024-04-10

- Typescript : is a strongly typed programming language,that builds on javascript with syntax for types.

#### Angular Framework

- Angular is a development platform, for building scalable web applications.
- Routing, form-management, client-server communication and more

Project Structure:

- `src`: project's root directory, source files and resources required to develop angular.
- `src/app/`: core functionality of angular application.Contains various components,services,directives and other resources.User interface,business logic and data manipulation.
- `src/assets/`: images,fonts,icons,favicons,json files,configuration files.
- `src/environment/`: environment specific configuration files.Different environments for development,staging,production.
- `node_modules`: node modules which are dependencies are stored in this folder
- `angular.json`: essential configuration file, customize project's build, development and deployment process like `ng build`, `ng serve` & `ng test`.

1. Component

Components are fundamental building blocks for creating applications in Angular.Angular built on component architecture to organize projects into organized parts.All the components are identified by the suffix `component` to a file name like `todo-list-component.ts`.

To render UI items, allowing users to interact with the application and access its functionality.
A component is like a class,where the element's behaviour and its view are encapsulated.

```javascript
@Component({
    selector: 'calculator`,
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.rss']
})
export class Calculator{

}
```

- selector: which element app's HTML component need to be used.
- templateUrl: identifying the template component will use.
- styleUrl: including component specifc CSS.


### 2024-04-11

- To create a new object in spring without using the `new` keyword,using `@Configuration` & `@Bean`

```java
@Configuration
@ComponentScan("com.example.account")
public class AppConfig{
    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    Accounts accounts(){
        return new Accounts();
    }
}
```

Now autowire RestTemplate or Accounts in the service layer, this is the standard way of creating new objects in spring using bean definitions

 - Rest Template

```java
@Autowired
RestTemplate restTemplate;

@Override
public AccountResponseDto addNewAccountCustomer(AccountRequestDto accountDto){
    String url="http://localhost:8081/api/v1/customer/{id}"
    ResponseEntity<CustomerDto> response=restTemplate.getForEntity(url,CustomerDto.class,accountDto.customerId());

}
```

- Filing RM bugs in debian
[1] https://qa.debian.org/howto-remove.html
[2] https://lists.debian.org/debian-qa/2004/09/msg00049.HTML


### 2024-04-15

- Debian

ABI: Application Binary Interface, how the compiler builds the application, abbreviation just like API.

There are two types of ABI changes: ones that are backward-compatible and ones that are not. An ABI change is backward-compatible if any reasonable program or library that was linked with the previous version of the shared library will still work correctly with the new version of the shared library.

- symbols files, a shared library are normally provided by the shared library package
- If a package or library depends on a shared library, when the package is installed, its libraries should also be installed.If the shared library implementation or behaviour is changed , to determine these either a symbol file or shlibs file is to be provided.
- symbols file: records every changes done to the shared library,the minimal version of the package any binary using this symbol will need
- shlibs file only documents the last time the library ABI changed%      



### 2024-04-16

- git tag: `git tag -a <tagname>`
- wrote a bash script for my site deployment

```bash
#!/bin/bash
hugo
tag=$(git describe --tags --abbrev=0)
message=$(git log $tag..HEAD --oneline --pretty=format:"%s")
echo $message
echo $tag

git clone git@github.com:vinay-keshava/vinay-keshava.github.io.git
cp -r public/* vinay-keshava.github.io && cd vinay-keshava.github.io
git add .
git commit -m "$message" -m "Tag: $tag"
git push --all
cd .. && rm -rf vinay-keshava.github.io

git tag $(date +"%d.%m.%Y")
```
