---
title: 02,February 2024
type: docs
sidebar:
  open: false
---

### 2024-02-12

- [Ant](/docs/java/ant/) : Java based make tool
	- Creating a simple ant build system for HelloWorld project.
	- Understanding of `build.xml`
	- Build commands of ant like `ant compile`, `ant jar`, `ant run`

- Java 17 features

	- JEP: Pseudo Random Generator
	- JEP: Switch Expressions
	- JEP: Records - for creating immutable object without the use of lombok
	- JEP: Usage of Objects in `switch` statement.
	- JEP: Updated `instanceof` method to avoid redundant casting
	- JEP: Local Variable Type Inference
	- JEP: Sealed Classes using `sealed` and `permits` keyword, to restrict the implementation and extension of classes and interfaces in java

	(JEP) - JDK Enhanced Proposal.


### 2024-02-14

- Termux Learnings

	- installing packages in termux `pkg install git`
	- `pkg update` for package updates.
	- `pkg upgrade` for package upgrades.
	- `pkg edit-sources` command to edit sources.list alike files,to change the default mirror


### 2024-02-15

Java 17 Features

[Link to the Java 17 docs](/docs/Java/java17.md)

- Sealed Classes & Interfaces
- Switch Statements
- InstanceOf Pattern Matching
- Switch Expressions
- Record Classes
- TextBlocks
- Strong Encapsulation of JDK Internal API’s
- Local Variable Type Inference


### 2024-02-18

- Nginx
	- To list files in nginx web server
	use `autoindex on` within the server TextBlocks


### 2024-02-19

- Conventional Commits

Defining a standard format for commit changes,which can be processed by automated tools to produce documentation.
```bash
<type>[(optional <scope>)]: <description>
[optional <body>]
[optional <footer>]
```
Link to [docs](/docs/hacks/conventional-commit/)

- Descriptive git commit messages.

    `git commit -m "Tittle" -m "Description"` , a descriptive git commit can be written by using the option -m twice


### 2024-02-19

- Project Lombok
    - [Link to the docs](/docs/java/lombok)
    - common annotations used in lombok
    - avoiding boilerplate code with lombok annotations
    - `@Getter`,`@Setter`,`@RequiredArgsConstructor`,`@Builder`,`@Data`,`@AllArgsConstructor`,`@NoArgsConstructor`


### 2024-02-26

- FFMPEG

ffmpeg is a suite of libraries and programs to handle video,audio & other multimedia files and streams.

```bash{filename="Joining multiple videos using ffmpeg"}
$ ffmpeg -f concat -safe 0 -i joinvideo.txt -c copy episode2.mp4

vinay@falcon:~/Documents/blog$ cat ../KumaraParvathaVideo/joinvideo.txt
file kp-peak-rest.mp4
file thick-forest.mp4
file rohan_vlog.mp4
```

Rotating a video by a certain degree using ffmpeg

```bash
ffmpeg -i inputfile.mp4 -vf "transpose=1" outputfile.mp4
```



