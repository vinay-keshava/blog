---
title: 01,January,2024
type: docs
sidebar:
  open: false
---

### 2024-01-02

- LVM -- Logical Volume Management.

	- Concepts of Physical Volumes,Volume Groups,Logical Volumes.
	- ```sudo lvmdiskscan```
	- Creating physical volume using physical drives

		```sudo pvcreate /dev/sda /dev/sdb```

	- Creating Volume groups
		```sudo vgcreate /dev/sda /dev/sdb```
	
	- Creating Logical Volumes within the volume groups

		```
		    sudo lvcreate -L 10G -n projects LVMVolGroup
		    sudo lvcreate -L 5G -n www LVMVolGroup
           sudo lvcreate -L 20G -n db LVMVolGroup
		```
- Golang
	- Package import,multiple package import
	- Exported names starting with capital letters.
	- Functions: function definition,function parameters,shortening function parameters,return types, named return types,

### 2024-01-03

1. LVM
	- Formatting and Mounting a logical volume

	```
	sudo mkdir -p /mnt/{backup,college-data}
	sudo mount /dev/VolGrpLvm/backup /mnt/backup/
	sudo mount /dev/VolGrpLvm/college-data /mnt/college-data/
	```
	- Renaming a logical Volume

	```
	sudo lvrename /dev/VolGrpLvm/college-data /dev/VolGrpLvm/college
	```

	- Extending a logical Volume
	```
	sudo lvextend -L+5G /dev/VolGrpLvm/backup ``` ```sudo lvextend -L25G /dev/VolGrpLvm/backup
	```

	-  Shrinking a logical volume
	```
	sudo lvreduce  -L20G /dev/VolGrpLvm/backup
	```

	- Removing an logical volume
  	 ```
   	sudo lvremove /dev/VolGrpLvm/backup
	```

	- LVM Stripping
	```
	sudo lvcreate -L 10G -i2 -I64 -n backup1 VolGrpLvm
	```

	- LVM Mirroring
	```
	sudo lvcreate -L 5G -m1 -n mirrorLV VolGrpLvm
	```

	- ThinPool and Thin provisioning
	```
	sudo lvcreate --thinpool vg_thin/thin_pool --name volume_1 --virtualsize 3G
	```
2. Golang
	- Variables
	- Short Description variables
	- Types
	- [Type conversion and Type inference](docs/golang/introduction/#type-conversion-and-type-inference)
	- [Constants](/docs/golang/introduction/#constants)


### 2024-01-04

 -  Network File System(NFS)

	- Here is the link to the [document](/docs/nfs)
	- Installing NFS server and setting directory permissions to 777
	- Configuring Export directory by editing the file ```/etc/exports```
	- Options to configure the NFS server,either readonly or read write, sync or async,subtree check.
	- Exporting the shared directory using ```exportfs``` command
	- Configuring firewall ```ufw``` to allow clients to access the shared folder.
	- Configuring client using nfs-common package
	- Creating mountpoints on the client side to access the shared folder.
	- Mounting the folder using mount command or adding the entry to ```/etc/fstab```

### 2024-01-09

 -  Ruby Learning

	- [Data Types](/docs/ruby/introduction/#data-types)
	- [Type Conversion](/docs/ruby/introduction/#type-conversion)
	- [String Interpolation](/docs/ruby/introduction/#string-interpolation)
	- [Strings](/docs/ruby/introduction/#strings)
	- [Basic Data Structures and Hashes](/docs/ruby/introduction/#basic-data-structures-arrays-and-hashes)
	- [Symbols](/docs/ruby/introduction/#symbols)
	- [Variables](/docs/ruby/introduction/#variables)
	- [Input Output](/docs/ruby/introduction/#input--output)

   Solved exercises of Odin Project for the [String Exercices](https://github.com/vinay-keshava/ruby-learning/commit/94b3203524badc98ba8c822d362b8fbe2f5ba2ea) & [Basic Data Types](https://github.com/vinay-keshava/ruby-learning/commit/a5801bada579481177c4a2a42c28553dcbd74fb5)

### 2024-01-18

  - Guake load preferences from config file
	- ```guake --restore-preferences ~/Downloads/guake_prefs ```
  - Python Introduction learing
  	- [Text](/docs/python/introduction/#text) - multi line print statement,escape sequences,strings and len() of strings
	- [Variables and Types](/docs/python/introduction/#variables-and-types)
	- [Lists](/docs/python/introduction/#lists)
