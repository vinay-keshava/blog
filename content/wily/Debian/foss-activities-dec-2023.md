---
title: "Foss activities - Dec 2023"
date: 2023-12-31
authors:
  - name: Vinay Keshava
    link: https://blog.vinay.im/about
    image: https://blog.vinay.im/images/vinay.png
description: "Foss Activities for the month of December 2023"
tags: ["osm", "debian", "oct"]
draft: false
comments: true
---

## Debian Activities

1. [javamorph](https://tracker.debian.org/news/1488467/accepted-javamorph-0020100201-4-source-into-unstable/)
    
    - Added compilation support for JDK 21 with the help of Vladimir Petko.
    - [#Bug 1052620](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1052620)
    - Link to [MR](https://salsa.debian.org/java-team/javamorph/-/merge_requests/2), MR accepted and bug closed.
    - Thanks to package maintainer tony mancill for sponsoring package.

2. [ruby-validates-hostname](https://tracker.debian.org/news/1484802/accepted-ruby-validates-hostname-1013-1-source-into-unstable/)

    - New Upstream 1.0.13-1
    - Routine Update
    - Required for gitlab 16.4

3. [ruby-gpgme](https://tracker.debian.org/news/1484811/accepted-ruby-gpgme-2023-1-source-into-unstable/)

    - New Upstream 2.0.23-1
    - Routine Update
    - Required for gitlab 16.4

3. [ruby-lockbox](https://tracker.debian.org/news/1488043/accepted-ruby-lockbox-130-1-source-into-unstable/)

    - New Upstream 1.3.0-1
    - Routine Update
    - Required for gitlab 16.4

4. [ruby-gitlab-experiment](https://tracker.debian.org/news/1485171/accepted-ruby-gitlab-experiment-080-1-source-into-experimental/)

    - ruby-gitlab-experiment to experimental
    - New Upstream version 0.8.0-1
    - Required for gitlab 16.4

5. [ruby-tanuki-emoji](https://tracker.debian.org/news/1485430/accepted-ruby-tanuki-emoji-070-1-source-into-experimental/)

    - New Upstream version 0.7.0-1
    - Required for gitlab 16.4

6. epm 

    - Updated d/watch patch for epm,epm upstream maintainer has been changed to github.com/jimjag/epm
    - [Bug #1051502](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1051502#10)

7. [ruby-loofah](https://tracker.debian.org/news/1490787/accepted-ruby-loofah-2214-1-source-into-unstable/)

    - New upstream version 2.21.4
    - Required for GitLab 16.6.x

8. [ruby-apollo-upload-server](https://tracker.debian.org/news/1491140/accepted-ruby-apollo-upload-server-215-1-source-into-unstable/) 
	
    - New upstream version 2.1.5
    - Required for Gitlab 16.6.x

9. [ruby-po-to-json](https://tracker.debian.org/news/1491146/accepted-ruby-po-to-json-200-1-source-into-experimental/)

   - New upstream version 2.0.0
   - Required for Gitlab 16.6.x
   - Refreshed Patches.


