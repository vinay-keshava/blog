---
title: "Foss activities - Nov 2023"
date: 2023-12-01
authors:
  - name: Vinay Keshava
    link: https://blog.vinay.im/about
    image: https://blog.vinay.im/images/vinay.png
description: "Foss Activities done for the month of November 2023"
tags: ["osm", "debian", "oct"]
draft: false
comments: true
---

## Debian Activities
1. [javamorph](https://tracker.debian.org/javamorph/)
    
    - Added compilation support for JDK 21 with the help of Vladimir Petko.
    - Link to [MR](https://salsa.debian.org/java-team/javamorph/-/merge_requests/2)

2. [ruby-pry-byebug](https://tracker.debian.org/news/1483592/accepted-ruby-pry-byebug-3101-1-source-into-unstable/)
    - My first QA upload to Debian
    - Closing the bug [#1054747](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1054747)
    - Link to [MR](https://salsa.debian.org/debian/ruby-pry-byebug/-/merge_requests/4)
    - New Upstream release 3.10.1

    Abhijith pointed out this bug to me, to prevent ruby-octokit from being removed from the archive.
    Thanks to Miguel Landaeta for sponsoring this package upload. 




