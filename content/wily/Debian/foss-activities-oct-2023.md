---
title: "Foss activities - Oct 2023"
date: 2023-10-18
authors:
  - name: Vinay Keshava
    link: https://blog.vinay.im/about
    image: https://blog.vinay.im/images/vinay.png
description: "Foss Activities done for the month of Oct 2023"
tags: ["osm", "debian", "oct"]
draft: false
comments: true
---

Post DebConf Month
## Debian Activities
1. [ruby-aws-sdk-core](https://tracker.debian.org/news/1470252/accepted-ruby-aws-sdk-core-31780-1-source-into-experimental/)
    
    - New upstream release 3.178.0

2. [rofi](https://tracker.debian.org/news/1472143/accepted-rofi-175-01-source-amd64-into-unstable/)

    My first NMU( Non Maintainer Upload) to debian

    Sponsorer: Abhijith PA,also thanks to karthik for mentioning to update rofi,because it was no longer maintained also fixed the bug https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1043483 for d/watch

    - New Upstream version 1.7.5
    
    - d/watch update closes [bug](https://bugs.debian.org/1043483)

    - Refreshing Patch to fix [man pages](https://salsa.debian.org/karthik/rofi/-/commit/1e10fc056cf0a151cad91839fb55d79b72c7f1fe)
    
    - Fixing Build Depends on [obsolute package](https://salsa.debian.org/karthik/rofi/-/commit/e234982e41fb05b023f1641e4f877db5d2f6890d)

3. [ruby-aws-sigv4](https://tracker.debian.org/news/1474830/accepted-ruby-aws-sigv4-161-1-source-into-experimental/) 

    - New Upstream release 1.6.1

4. [ruby-aws-sdk-s3](https://tracker.debian.org/news/1475369/accepted-ruby-aws-sdk-s3-11300-1-source-into-experimental/)

    - New Upstream release 1.130.0

## Open Street Map

My edits on open street Map for October in and around Bangalore and Pavagada.
    https://www.openstreetmap.org/user/vinay-keshava/history


```
    :wq
```

