---
title: "[HomeLab] Cups, Print Server on Pi"
date: 2024-09-09
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "Homelab, setting a print server using cups on Raspberry Pi 4"
tags: ["cups", "hplip","raspberrypi"]
comments: false
---
#### Cups
[CUPS](https://openprinting.github.io/cups/), Common Unix Printing System manages printing services on linux. Cups uses IPP( Internet Printing Protocol) for managing print jobs. Cups also provides web interface, command line interface to manage printer and print jobs.

I wanted to setup a print server at home on a raspberry pi under local network, instead of connecting the usb to my laptop everytime which is annoying. I have a [HP Laserjet M1136 Professional Pro]( https://support.hp.com/ro-en/product/details/hp-laserjet-pro-m1136-multifunction-printer-series/5094778 ) printer at home which requires propreitary drivers to be installed on linux,my printer is not listed in [openprinting](https://openprinting.org/printers/manufacturer/HP).
<img src="/images/cups-printer-server.jpg" alt="HP Laserjet M1136 Printer" style="width:450px;"/>

#### hplip
[hplip](https://developers.hp.com/hp-linux-imaging-and-printing), a utility for hp printers to install drivers and setup the printer on the machine.

Install hplip on raspberry pi,
```bash
sudo apt install hplip
```

hplip depends on cups, not needed to install it externally.

Due to [disabled root access issue](https://unix.stackexchange.com/questions/627597/hplip-cannot-be-installed-on-system-with-disabled-root-user-password-incorrect) root password fails, switch to root user using `sudo su`, and run `hp-plugin` to download all the proprietary plugins 


```bash
root@raspberrypi:/home/vinay# hp-plugin

HP Linux Imaging and Printing System (ver. 3.22.10)
Plugin Download and Install Utility ver. 2.1

------------------------------------------
| PLUG-IN INSTALLATION FOR HPLIP 3.22.10 |
------------------------------------------

  Option      Description
  ----------  --------------------------------------------------
  d           Download plug-in from HP (recommended)
  p           Specify a path to the plug-in (advanced)
  q           Quit hp-plugin (skip installation)

Enter option (d=download*, p=specify path, q=quit) ? d
```

Enter option `d` to download the required plugin for the printer

```bash
Do you accept the license terms for the plug-in (y=yes*, n=no, q=quit) ? y
```

Input `y` to accept terms and conditions

Exit the `sudo su` terminal session and run `hp-setup -i` as a normal user.

```bash
$hp-setup -i

HP Linux Imaging and Printing System (ver. 3.22.10)
Printer/Fax Setup Utility ver. 9.0

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.

(Note: Defaults for each question are maked with a '*'. Press <enter> to accept the default.)


--------------------------------
| SELECT CONNECTION (I/O) TYPE |
--------------------------------

  Num       Connection  Description
            Type
  --------  ----------  ----------------------------------------------------------
  0*        usb         Universal Serial Bus (USB)
  1         net         Network/Ethernet/Wireless (direct connection or JetDirect)

Enter number 0...1 for connection type (q=quit, enter=usb*) ? 0
Using connection type: usb

Setting up device: hp:/usb/HP_LaserJet_Professional_M1136_MFP?serial=000000000QHBYPM0PR1a
```
I have my printer connected to Pi via USB, so choosing `0`

```bash
$ sudo usermod -aG lpadmin <<USER>>
$ sudo cupsctl --remote-any
$ sudo /etc/init.d/cups restart
```

Add the user to lpadmin and enabling remote printing within the network using `cupsctl`.

https://192.168.1.100:631/admin/ cups web interface

<img src="/images/cups-1.png" alt="Add a new printer in CUPS web UI" style="width:650px;"/>
Select HP Laserjet Pro M1136 connected via USB, and then input the root password of raspberrypi.

<img src="/images/cups-5.png" alt="vinay" style="width:650px;"/>
<img src="/images/cups-4.png" alt="vinay" style="width:650px;"/>
Printer added successfully, you can now manage print jobs via UI also add printers.

Now my sister and dad can print documents from phone too (:
<img src="/images/cups-6.jpg" alt="Print documents from phone" style="width:250px;"/>
