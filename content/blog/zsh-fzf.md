---
title: "Zsh Zen + FZF"
date: 2024-07-26
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "Configuring Zsh with fuzzy search "
tags: ["fzf", "zsh"]
comments: false
---
Terminal emulator is like a home,every linux user wants to customize their terminal either fonts,appearance etc.
I was a `bash` user for all these years, and switched to `fish` a few months ago, until i found the zsh and zimfw.

I used [ohmyzsh](https://ohmyz.sh/) along with zsh for good theme.I use a default ohmyzsh theme.

`sudo apt install zsh zsh-autosuggestions zsh-syntax-highlighting`

Installing `zsh-autosuggestions` and `zsh-syntax-highlighting` packages to enable autosuggestions and syntax highlighting for zsh.
Append the below lines to `.zshrc` to enable autosuggestions and syntax highlighting.

```bash(filename=".zshrc")
source "/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
source "/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh"
```

[Fuzzy Finder](https://junegunn.github.io/fzf/), is a command line tool to find previously executed commands.
To enable fzf with zsh, add the following in `.zshrc` after installing fzf.

```bash(filename=".zshrc")
source "/usr/share/doc/fzf/examples/key-bindings.zsh"
source "/usr/share/doc/fzf/examples/completion.zsh"
```

Below is the image after fzf configuration with zsh.
<img src="/images/zsh-fzf.png" alt="zsh and fzf configuration" style="width:800px;height:350px;">

Thanks (:
