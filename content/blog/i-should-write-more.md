---
title: "I Should Write more!"
date: 2023-11-04
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "I now love writing"
tags: ["posts", "writing", "blog"]
comments: true
---
2023 is about to end in another few months,I feel ,i should write more now!.
I just love writing now,I feel i should write more to express my thoughts,ideas,its been since few months i started to write more [docs](/docs),and i just simply love it.


I feel like it helps me document stuff,check how my learning evolved over time and its progress.

At first when i started to create my portfolio i was reluctant to write much,barely one or two articles per year,i was not confident about my grammer too,now i really dont care about it,i just write my raw thoughts.There's some level of satisfaction of writing,i encourage others also to write more.

People who read my blog might provide feedback too,correct if any mistakes.
I also encourage other people to write, if they are too shy they can try anonymous blog. 

```
    :wq
```

