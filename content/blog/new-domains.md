---
title: "New Domains"
date: 2024-01-16
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "New Domains"
tags: ["posts", "writing", "blog"]
comments: true
---

So finally i bought two new domains,one for myself and another one for my dad.After  days of hunting for the most creative domains and its availability,i have registered `winay dot in` from gandi.net
inspired from [Vishal Arya](https://wisharya.com/),i always wanted to buy a domain for myself,had been hunting for the creative one since college, and finally i registered one.
I have also bought another domain for my dad `smpdiesel dot in`,which is a diesel generator service based company.I was able to convince my dad to use GNU/Linux, he now uses Debian 12 on this PC, and its super speed with low specs.In coming days i will configure the DNS to this site, and self host few applications for my personal use. 

[Update: Feb 27,2024 the interesting part of the domain winay.in was the `w` consists of two `v`'s ,so its `vvinay.in` ]

[Update March 28,2024 https://smpdiesel.in is up and running built with hugo static theme,and finally i've setup a website for my dad yayyyy!  ]
