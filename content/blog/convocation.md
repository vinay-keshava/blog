---
title: "Convocation,Bachelor of Engineering 🎓"
date: 2024-04-12
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "Graduated in Computer Science & Engineering from Alva's Institute of Engineering & Technology"
tags: ["aiet", "engineering", "convocation"]
comments: true
---

Now graduated from [Alva's Institute of Engineering & Technology](https://aiet.org.in) in Computer Science!!
On 06th April 2024 approximately an year after our farewell, our [college](https://aiet.org.in) conducted convocation to distribute our degree certificates :-)

We all were excited to meet our friends,its been almost an year we met everyone!
It was a scorching summer saturday in mangalore, had to report to college at 9:00am to collect my semester marks card and degree certificate.Chinmay picked me up from mangalore.After clearing our no dues we received our marks card.

We had time till afternoon for the program to start, had a quick breakfast at moodbidri and headed to Kanchi bail waterfalls near Gundyadka, just to find out that the water has been dried out.

<img src="/images/convocation.jpg" alt="Graduated Me!!" style="width:230px;height:370px;">
That's me sweating because of the humidity. Clicked few pictures with my friends and said them bye,would meet them in bangalore.

The next day,we visited Swamy Koragajja Aadisthala Temple, Ullal Mangalore & Kadri Manjunatha Temple with Ganesh and Abhishek, and then we started heading towards Kundapura.We visited the Kapu Beach Lighthouse afternoon, sadly we couldn't play in water,it was too humid, barely able to walk or sit beside the beach.
We were trying to stay hydrated as much as possible to beat the heat.

<img src="/images/kadri.jpg" alt="Kadri Manjunath Temple" style="width:260px;height:470px;">
Picture: Kadri Manjunath Temple

<img src="/images/kapu-beach.jpg" alt="Kapu Beach" style="width:260px;height:380px;">
Picture: Kapu Beach and Lighthouse
<img src="/images/kapu-group.jpg" alt="Kapu Beach" style="width:340px;height:190px;">
Picture: [From Left] Abhishek,Ganesh & Me

Had our lunch at Eshanya Restaurant in Kundapura and they dropped me at the bus stop, i had to go back to mangalore to catch the train to reach bangalore. I was just accompanying my friends till Kundapura,because i had no work or plans till evening. Waved a bye at Ganesh & Abhishek and came back to mangalore by express bus.

Met darshan and manoj at the City Centre Mall, we all were boarding the same train.We waited for Megha and Nadiya to join us who were also boarding the same train.
I still had a few more hours of time left to catch the train.The seat was not confirmed it was in waiting list,had been checking it since morning and finally an RAC was confirmed.

Maybe will write another blog post to tell about engineering and hostel experience.

Until then Bye!!!

