---
title: "Lap'23"
date: 2024-05-31
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "My another year of existence on earth"
tags: ["23", "birthday"]
comments: true
---

Cheers to 23,my another year of existence on this planet.
A wonderful day filled with wishes from friends and family.

```
Time Flies. Time Flies faster every year.
Time Flies whether you're having fun or not, 
whether you're living your life big or small,
whether you surround yourself with fear or Laughter - Claire Cook
```

Its exactly been an year since i was graduated from my bachelor's degree.Last year at this point of time, was with my friends for the last trip of engineering.We had our convocation last month,after months of graduating. Wrote a separate blog post about attending the college [convocation](/blog/convocation).
These days the weekends is mostly to meet up college & hostel friends to chit chat about how we miss our good old days :)

Yayy i started reading books,the first book i read was `When Breath Becomes Air` by Paul Kalanithi gifted by my friend.Currently reading `IKIGAI` the japanese way of living life.I also wish to write a summary/review of the books i read if i find time over weekends.I also started loving to write more blog posts about the things happening.

I've messed up my [Debian Developer process](https://nm.debian.org/process/1243), i regret it and i hope to contribute more time to debian and i wish to become one atleast in the coming year.

I also have a huge pending list of tasks to be done,mostly learning stuff.

Lastly thanking family & friends for supporting me.

Bye!!!
