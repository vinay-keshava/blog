---
title: "WILY"
date: 2024-03-04
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "Logging all my daily learnings"
tags: ["logs", "learnings", "blog"]
comments: true
---

### What I Learnt Yesterday(WILY)

WILY is a name I rephrased to log all my daily learnings.I started this two months ago, where I log about what I learnt yesterday.Everyday is a learning day it can be tech, some commands or some life lessons experienced throughout the day.

Inspired from  [Zettelkasten](https://writingcooperative.com/zettelkasten-how-one-german-scholar-was-so-freakishly-productive-997e4e0ca125)


At some point of time in our lives we would want to relearn a few things after a few days/weeks but the effort to find resources for a specific topic can be time consuming.
I learnt a lot of stuff during engineering but I don't remember any thing now.

I was using Notion as my documentation app, used to make notes within a notion workspace,it has an option to export to PDF and also share an article with a public link.
Then I found this beautiful hextra hugo theme, where i can design to my custom look and needs. Took a few weeks to setup this theme locally,with the help of the hextra  theme maintainer on GitHub.
Now everytime I learn something new i write and save it in a markdown file, so thats its easy to add back to the docs.
I also love the `docs` feature of this theme,making it beautiful and easy to search.

I wish to log all my daily learnings with more sincere effort.

See you until next time.


