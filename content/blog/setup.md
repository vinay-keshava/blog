---
title: "Lap'23"
date: 2024-05-31
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "My another year of existence on earth"
tags: ["23", "birthday"]
comments: true
draft: true
---
Iosevka font
https://blog.programster.org/install-iosevka-fonts
wget -O fonts-iosevka.deb \
  http://phd-sid.ethz.ch/debian/fonts-iosevka/fonts-iosevka_22.0.0%2Bds-1_all.deb \
  && sudo dpkg -i fonts-iosevka.deb \
  && rm fonts-iosevka.deb \
  && sudo fc-cache -fv

Allow pages to choose their own fonts, instead of your selections above -> in firefox 
uncheck the above option and choosed iosevka for all fonts 


alacritty
```
➜  ~ cat .config/alacritty/alacritty.yml 
font:
  normal:
    family: Iosevka
  size: 13.0

window:
  padding:
    x: 18
    y: 14

➜  ~ 
```
xmodmap -pke >  ~/.Xmodmap


remove Lock = Caps_Lock
keysym Caps_Lock = Control_L
keysym Control_L = Caps_Lock
add Control = Control_L


add the above lines to .Xmodmap and launch again  using `$ xmodmap .Xmodmap`



## zsh oh-my-zsh and fzf configuration

sudo apt install zsh zsh-autosuggestions zsh-syntax-highlighting

#### Enable syntax highlighting and autosuggestions
source "/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
source "/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh"



enabling fzf integration into zsh, add the following the .zshrc after install fzf
source "/usr/share/doc/fzf/examples/key-bindings.zsh"
source "/usr/share/doc/fzf/examples/completion.zsh"



### neovim setup

- [Lazyvim](https://lazyvim.org) clone the repository to `.config/nvim`  
for copy text to clipboard use the command `"+y`


### Hibernation setup

`sudo apt install pm-utils`
 the command pm-hibernate, will hibernate the machine
