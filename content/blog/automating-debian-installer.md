---
title: " Automating the Debian Installer"
date: 2023-10-18
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: " Automating the Debian Installer with the help of preseed.cfg file "
tags: ["debian-installer", "debian", "preseed.cfg file"]
comments: true
---

The Debian Graphical Installer,when i was introduced to Kali Linux(a Debian based distro),during my initial days of bachelor's, the installer was so tricky to install GNU / Linux,thereby losing my data (:

Even though nowadays, Live systems have [Calamares](https://calamares.io/)  based installer,i feel the Graphical Installer is a bit annoying,so i wanted to automate the debian installer using a preseed configuration file,giving answers to debian installer in a config file making it easier and faster.

This is the wiki link for Debian Installer Preseeding https://wiki.debian.org/DebianInstaller/Preseed .
Preseeding is  like a set of answers to debian-installer(d-i)  questions.Preseeding can be done in 3 ways 

1.Adding the preseed file to installer's initrd.gz

2.Over webserver via DHCP

3.Loading the preseed file from a webserver over the boot options.

This blog post explains automating the debian installer by loading the preseed file from a webserver over the boot.

When Graphical Installer boot menu appears after booting through the Debian iso,select the  ```Help``` entry from the menu.
![AutomateDebianInstaller1](/images/automate-debian-installer-1.png)

After selecting the ```Help```  entry from boot menu,a ```boot:```  prompt is displayed below, enter you webserver url of the preseed file, in my case the preseed file was on my server with ip ```http://144.24.135.168/preseed.cfg/```, and give ``` boot: auto url=http://144.24.135.168/preseed.cfg``` and  the ```auto```  command launchs the automated debian installer. 
![AutomateDebianInstaller2](/images/automate-debian-installer-2.png)

The default preseed file for new release will be updated here https://wiki.debian.org/DebianInstaller/Preseed#Default_preseed_files
Answers like setting username and password for Account setup,Network Configuration,Mirror Settings,Partitioning disks,setting normal user as root user Locales,Keyboard layout,Package Selection and many more

All the features in the Debian installer can be answered with the preseed file.
![AutomateDebianInstaller3](/images/automate-debian-installer-3.png)


``` bash{filename="My Preseed file"}
d-i mirror/http/hostname string http.us.debian.org
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string
d-i passwd/root-login boolean false
d-i passwd/user-fullname string Vinay Keshava
d-i passwd/username string vinay
d-i passwd/user-password password vinay
d-i passwd/user-password-again password vinay
d-i clock-setup/ntp boolean true
d-i partman-auto/init_automatically_partition select biggest_free
``` 
In the above preseed file i've set the debian mirrors [ line 1-3 ]with ```http.us.debian.org```  and the directory for the mirror is ``` /debian```,Account setup with password[ line 4-8 ], [ line 9 ] for ntp server to sync time with and [ line 10 ] for automatically partitioning the disk with biggest free space disk partition.

This is sample preseed file with few options and it can be customized accordingly.


```
    :wq
```

