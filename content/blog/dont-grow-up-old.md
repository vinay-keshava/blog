---
title: "Dont Grow Up Old!"
date: 2023-12-20
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "Dont grow up old,its a myth"
tags: ["posts", "writing", "blog"]
comments: true
---

Don't grow up old, everyone wants to become an adult soon, enjoy life ,stay independent,take more responsibilites etc etc.

But in this process, you miss out a lot of things,the excitement to grow old faster kills you, and you will only realize this when you look behind and regret saying, i should have done this at that point of time in my life, but you cannot do it now,and now do nothing but,regretting.


Don't let the kid inside you miss out the fun,enjoy every moment in life to the fullest,enjoy by living the present moment.
Do what you're supposed to do at that point of time in your life.
What ever is supposed to happen,happens at the right time,irrespective of what and how many times you think about it.
