---
title: "DebConf 23,Kochi India"
date: 2023-11-04
authors:
  - name: Vinay Keshava
    link: /about
    image: /images/vinay.png
description: "My Experience of attending DebConf23,Kochi from Sep10-Sep17"
tags: ["kochi", "debian", "debconf"]
comments: true
---

Its almost been more than a month since [DebConf23](https://debconf23.debconf.org/) happened,and this post was in draft for very long time because i just wanted to write the best post of all my post, and finally writing about the experience of attending a FOSS conference/conference for the first time.I had graduated in early May'23, so was not sure whether i could attend DebConf.I had joined [Deeproot GNU/Linux](https://deeproot.in/),just before DebConf as an Intern.
I was excited for this because i meeting my Debian and [Free Software Community of India](https://fsci.in) friends for the first time in 2 years,i hadn't met anyone physically,was conversing with them online.

I became a [Debian Maintainer](https://wiki.debian.org/DebianMaintainer) in early'2023 March ,special thanks to [Praveen](https://poddery.com/u/praveen) my GURU,for advocating my Debian Maintainer process,who taught me Debian Packaging from scratch,answering my stupid questions and [Nilesh](https://nileshpatra.com/) for signing my keys.
I have written a separate [blog post](https://blog.vinay.im/blog/debian-maintainer/) about my experience of becoming a DM.

<img src="/images/ravish-debconf.jpg" alt="image" style="width:400px;height:auto;">

Ahh so this is [Ravish](https://ravish0007.github.io/) [Left] and me. Ravish is my college super senior,we were part of FOSS club called Edwin's Lab in our college,he's the one who introduced me to GNU/Linux. Forever indebted to ravish its because of him i'm working at DeeprootLinux,Thanks man.

It was my first time flying,I,[Abhas - Founder of Deeproot Linux](https://abhas.io/),Ravish boarded a cab to the Bangalore Airport, and we reached around 8:00pm,the architecture of [KIAL](https://www.bengaluruairport.com/) was just amazing,i was astonished, we had huge baggage to carry for the [Mostly Harmless](https://mostlyharmless.io) Stall at the airport,so we had rigourous security checks at the airport. 
We boarded the flight on 9th September, DebConf was about to start from 10th September [ I missed DebCamp ]

<img src="/images/debconf-flight.jpg" alt="DebConf-Flight" style="width:400px;height:300px;">

It was a surreal experience of flying for the first time,it was amazing feel.We also had our dinner (Veg Roll) on the flight.
Had the best view of city during the night time,watching'em from higher altitude was just so satisfying for eyes.
We reached Kochi Airport around 11:15pm and boarded a cab to Four Points Sheraton Hotel.
We reached Infopark Kochi around 12:30am and checked in to our hotels.I had applied for Accomodation and Food Bursary and it was approved :)
So my room was at [Four Points Sheraton Kochi](https://www.booking.com/hotel/in/four-points-by-sheraton-kochi-infopark.html),6th floor, also i was wondering who my roomate was,the earlier day i had receieved a mail that [Kurian Benoy](kurianbenoy.com) was my roommate, i was curious to meet him.

The next morning was DebConf Opening Ceremony,so had to sleep because it was already 3:00am i was exploring my hotel room.

<img src="/images/debconf.jpg" alt="DebConf" style="width:400px;height:auto;">

Woke up around 8:00 am and freshened up and headed to FrontDesk for Registration,we got our name with GPG key printed on our ID's,also got the SwagBag,with some cool stickers and swags by Debian.
After the registration i met kurian benoy, my roommate for a week.

<img src="/images/stickers.jpg" alt="DebConf-Stickers" style="width:400px;height:auto;">
Stickers part of Debian Swag


I and ravish were waiting at the HackLab for the opening ceremony,in mean time we met urbec,part of Video Team,had some good conversation too.

DEBCONF-23 starts with opening ceremony at Anamudi(These were the hall names,other hall names were Kuthiran,Ponmudi) the DebConf organizing team were on the stage giving a Welcome Note, and then after welcoming ceremony i went to attend talks which we interesting.
The talk schedule was not very tight, because we used to gather more at the eating place to meet new people from diverse countries,having some really good conversations.

I met praveen,shruti,anupa,ravi,sahil,abhijith,nilesh,bilal,suman,kelvin,pushkar,utkarsh,joostvb,Israel Galadima... and lot more people whom i wanted to meet.

I also met Pushkar from canonical, who used to stay at Bengaluru in the same locality and that was coincidence,meeting him was great too.
First day ended good.

![Debconf Praveen Ravi](/images/debconf-group-photo-praveen-ravi.jpg)
<figcaption>A picture with Pirate Praveen and Ravi.</figcaption>


<img src="/images/abhas-talk-debconf.jpg" alt="Abhas DebConf talk" style="width:400px;height:auto;">
Abhas's talk on Home automation with his amazing LED pant :) 



<img src="/images/debconf-cheese-and-wind-party1.jpg" alt="Cheese and wine party" style="width:auto;height:auto;">

<img src="/images/debconf-cheese-and-wind-party2.jpg" alt="Cheese and wine party 2" style="width:auto;height:auto;">

Cheese and wine party, the party is simple to bring good stuff from other countries like cheese and wine, People from all over the world bought different stuff to the cheese and wine party,we got the chance to taste from Korean wafers and red wine.


![Debconf DayTrip](/images/debconf-daytrip.jpg)
On 13th Sep there's a Day trip,but i hadn't registered early, only for Bird Sanctuary the registration was open,just the day before day trip edited the wiki and added my name.
A Day trip to [Thattekad Bird Sanctuary](https://wiki.debian.org/DebConf/23/DayTrip/#Tour_D:_Destination_Wilderness:_Jungle_Walk_at_Thattekad_.28Early_Morning_Departure.29) On the way to Thattekad i met /su/bin/ siby, on the bus.
Thattekad Bird Sanctuary is around 50km from the kochi,so we left around 7:00 am 

We reached around 9:00 and had snacks and started walking towards the bird sanctuary. 
It started with a walk around the bird sanctuary spotting for birds around,during the trek i was with Praveen,Bady,Kannan and Kelvin.
Spotting birds was difficult because it wasn't the actual season for migrating birds,so we could spot only few.
Kelvin was explaining how he started to contribute to  Open Street Map.

On our way back to the bus it started raining heavily and we were drenched completely,
The [cool debian umbrella](https://aana.site/@subins2000/111057301872140828) with Debian symbol on it,helped us to get back to the bus.After a hot tea and snacks we started back to kochi.

<img src="/images/debian-in-the-cloud.jpg" alt="Debian Cloud" style="width:350px;height:700px;">

Debian and Clouds :)

![Mostly Harmless Stall](/images/mostly-harmless-stall.jpg)
The place were i stayed the most Mostly Harmless Stall (But not Harmless) with Abhas,Akshay explaining people about Liberated Hardware,and why it is important in life to use Liberated Hardware.Enjoyed Sahil's touch typing and testing keyboards,i couldn't take a picture with Sahil,he also volunteers at FSCI managing instances,he taught me [git.fosscommunity.in update](https://blog.vinay.im/blog/git-fosscommunity-in-update) generating keys etc he's also a Debian Developer,part of DebConf organizing committe too, also met saswata from Unmukti Tech.

It was an Amazing conference,it was my first Conference/FOSS conference enjoyed it,got to meet new people,the ecosystem of the Hacklab to hack around,seeing the community bonding even ravish wanted to contribute by packaging so taught him packaging at hacklab during free time,Also i learnt about OpenStreetMap,and now i map in and around Bangalore in my free time,a good hobby that i'm interested.Enjoyed ravi's humourous jokes :) throughout DebConf

The only disappointing thing at DebConf was the demise of [Abraham Raji](https://abrahamraji.in/) during a Day Trip,[Debian project mourns the death of abraham](https://www.debian.org/News/2023/20230914), i hadn't personally met him,but conversed online about fsci camp,an inspiring soul

Abraham Lives Onnn!
