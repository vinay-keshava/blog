---
title: Rust
type: docs
next: /docs/selfhosting/nextcloud/
sidebar:
  open: false
---
Learning RUST programming language and documenting my learning

### Hello World - Rust !
```rust{filename="hello_world.rs"}
fn main(){
	println!("Hello world")
	}
```

To compile rust code

```bash
$ rustc hello_world.rs
```

Run the binary

```bash
$ ./hello_world
```

### Cargo

cargo is rust's build tool and package manager, to manage dependencies and its versions,basically crates.
cargo uses TOML format stands for Tom's Obvious Minimal language,which is cargo's configuration format.

#### Cargo useful commands

```rust
$ cargo new hello_cargo --vcs=git
$ cd  hello_cargo
```
on creating a new cargo project the source files are to be present within the ```src/``` directory and also ```Config.toml```  file is generated.


1. cargo build

```bash
$cargo build
   Compiling hello_cargo v0.1.0 (/home/vinay/learn/rust/hello_cargo)
    Finished dev [unoptimized + debuginfo] target(s) in 0.17s
```
a ```target/``` directory is created 

the binary is within the ```target/debug``` directory

```bash
$ ls target/debug/
build  deps  examples  hello_cargo  hello_cargo.d  incremental
```

```bash
$ ./target/debug/hello_cargo
Hello, world!
```

2. cargo run

```bash
$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.00s
     Running `target/debug/hello_cargo`
Hello, world!
```

instead of two commands of compiling and running the binary, ```cargo run``` is a simple command,which compiles and runs the binary


3. cargo check

compiles the code but does not produce an executable.A

```bash
$ cargo check
    Checking hello_cargo v0.1.0 (/home/vinay/learn/rust/hello_cargo)
    Finished dev [unoptimized + debuginfo] target(s) in 0.02s
```
