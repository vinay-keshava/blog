---
type: docs
title: "Conventional Commit"
---

### Conventional Commits

Defining a standard format for commit changes,which can be processed by automated tools to produce documentation.
```bash
<type>[(optional <scope>)]: <description>
[optional <body>]
[optional <footer>]
```

- type
    1. `build`: build system and dependencies
    2. `change`: changes to the implementation of existing feature
    3. `chore`: 
    4. `ci`: continous integration or continous delivery scripts
    5. `deprecate`: deprecate existing featuresA
    6. `docs`: adds,updates,revises documentation related to applcation
    7. `feat`: new feature
    8. `fix`: commit to fix defect in the application
    9. `perf`: increase performance of algorithm,without fundamental changes in existing feature
    10. `refactor`: refactor existing code,does not change existing features
    11. `remove`: remove a feature
    12. `revert`: revert one or more commits
    13. `security`: resolves security issue.
    14. `style`: reformats the style of source code
    15. `test`: automated tests of the product/application

- Description
    very short summary of the intent,or title for the body

- Body
    additional details in the commit,which can include multiple paragraphs  to describe the change.

- Footer
    `Close`,`Closes`,`Closed`,`Fix`,`Fixes`,`Fixed`,`Resolves`

Automated Version Numbering
`fix`: increase the patch number according to semver.org
`feat`: reset patch version and increase minor version, `1.2.4` changes to `1.3.0` patch number is reset.
`BREAKING CHANGE`: presence of BREAKING CHANGE header increases Major version


 - `git log --oneline` - prints out the subject in one line
 
 - `git shortlog` - prints one line subject and user

 - Subject lines should be capitalized


 ### References
1. https://www.conventionalcommits.org/en/v1.0.0/
2. https://cbea.ms/git-commit/
