---
type: docs
title: "Customize .qcow2 image"
---

Tired of Debian Installer ):

On Proxmox VE i had to go through the Debian Installer if i want to spin up a new VM,taking a lot of time and effort.

```bash
$ sudo apt install qemu-system libguestfs-tools
```

Download libguestfs-tools and qemu-system to modify the password and resizing the image.

1. Download the .qcow2 image from cloud.debian.org

https://cloud.debian.org/images/cloud/bookworm/20230910-1499/debian-12-genericcloud-amd64-20230910-1499.qcow2 ,this is a generic cloud image which can be easily imported onto proxmox.

2. Reset the root password on the disk image

```$ virt-customize -a debian-10-genericcloud-amd64.qcow2 --root-password password:debian```

By default the .qcow2 doesn't have any root password,so the disk image has be customized using virt-customize to add root password. 

3. Increase size of .qcow2 disk image.
By default the size of Debian Generic Cloud is 2GB, using qemu-img we can resize the disk image. 

```
qemu-img resize image.qcow2 +SIZE 
```

4. Import the disk image on Proxmox VE.

Copy the image to ```/var/lib/vz/template/qemu/```.
Create a VM on Proxmox VE without any media (do not attach any physical media) and delete any existing disk on proxmox.

```
qm importdisk 114 /var/lib/vz/template/qemu/debian-12-genericcloud-amd64-20230910-1499.qcow2 amogha -format raw
```

Execute the above ```qm importdisk``` on the proxmox server where ```114```is the VM id where in your case will be different.
Refreshing the Proxmox GUI on the browser,attach the ```unused Hard Disk``` under ```Hardware```, also add a cloudInit drive and set ```IP address to dhcp``` to automatically assign IP address for both IPv4 and IPv6.

Under ```Options``` update the boot order and check whether the hard disk which was added to be checklisted and prioritize it to first.

Another alternative way is to use Preseed file at boot which automates 
debian installer,haven't tried that yet.

5. Creating a new VM 

Creating a vm using command line on proxmox without using using the WEB-UI

```bash
#!/bin/bash

function newqemu_vm
{
qm create $vmid --name $name --memory $ram --net0 virtio,bridge=vmbr0
qm importdisk $vmid /var/lib/vz/template/qemu/debian-12-genericcloud-amd64-20230910-1499.qcow2 amogha -format qcow2
sleep 5
qm set $vmid --scsihw virtio-scsi-pci --scsi0 amogha:vm-$vmid-disk-0
qm set $vmid --ide2 amogha:cloudinit --boot c --bootdisk scsi0 --serial0 socket --vga serial0
qm set $vmid --ipconfig0 ip=dhcp
qm resize $vmid scsi0 +"$size"G
qm set $vmid --sshkey  ~/.ssh/id_rsa.pub
qm start $vmid
}

echo "HostName of the VM:"
read name
echo "Enter new VMID:"
read vmid
echo "Enter number of CPU cores:"
read cpucores
echo "Ram:"
read ram
echo "Enter Total Storage of the VM: [Example: +11G ]"
read size
newqemu_vm

```


:wq #for now
```
