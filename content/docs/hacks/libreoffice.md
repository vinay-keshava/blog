---
type: docs
title: "LibreOffice Shortcuts"
---


### Image as Watermark

1. Click `Insert` in the menu bar of LibreOffice Writer, and click on image.

2. Extend the image to full page and right click on the image and select `Wrap` option and then select `In Background`


### QR Code 

1. Click `Insert` in the menu bar, next click `OLE Object`

2. Next click on QR code and put the URL/Text in the field and click on `OK` to generate the QR code

### Variable Data and Time
