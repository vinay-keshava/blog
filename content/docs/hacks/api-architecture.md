---
type: docs
title: "API Architecture"
---

SOAP,REST,GraphQL and gRPC are most popular API architecture, in this digitizing world where large amounts of data is being transferred

#### REST

REST stands for REpresentational State Transfer, is a widely used architectural style for building web API's. REST API's uses HTTP requests to retreive and manipulate data. Key characteristics of REST include:

- Resource Based: REST API's are based on concept of resources,which are exposed through URL's.
- Stateless: REST API does not maintain the state between two requests,hence stateless.
- CRUD: REST API's support Create,Read,Update and Delete operations for manipulating data.
- Flexible: REST API's are flexible,used for many purposes in general.

Advantages:

- Caching: support caching,which improves performance and reduce server load.
- Simplicity: Easy to understand consists of simple concepts 
- Widely Adopted: adopted my many web development projects.

Disadvantages:

- Overfetching/Underfetching: REST API's suffer from overfetching and underfetching which impacts the performance
- Versioning: Versioning of API's can be challenging, changes to API can be client applications.


#### GraphQL

GraphQL is query language for API's, which is more flexible than REST. Allows clients to specify exactly what data they need, which reduces the data transferred over network.Key characteristics:

- Strongly Typed: clients and servers agree on the structure of the data exchanged.
- Query Based: clients specify exactly what they need and receives only that data.
- Single Endpoint: GraphQL has single endpoint,which simplifies client-server communications
- Schema driven: API schema defines the type of data available and operations that can be performed on that data.

[GraphQL uses `POST` HTTP method for all the operations be it GET,POST etc it uses POST HTTP method.](https://stackoverflow.com/questions/59162265/why-are-graphql-queries-post-requests-even-when-we-are-trying-to-fetch-data-and
)

Advantages:

- Reduced network traffic: due to less number of data transferred, network traffic is reduced. 
- Versioning: versioning is easier to manage in GraphQL, changes to API schema can be managed more easily.
- Strongly Typed: due to type of data being transferred.

Disadvantages:

- Caching: caching of graphql API's can be challenging,data requested by clients is highly specific 
- Learning Curve: difficult to learn


#### gRPC

gRPC Remote Procedure Call open source framework developed by Google for building Remote Procedure Calls API's.Unlike traditional API's that rely on HTTP methods,gRPC uses more efficient and lightweight Protocol Buffers as the datainterchange format and it supports multiple programming languages.

Using protocol buffers,faster and efficient communication is achieved.gRPC supports bidirectional streaming real-time  communication between clients and servers.

gRPC has strong support for code generation, generates a well-defined contract and generates client and server code in multiple languages making it easier for developers.
GRPC APIs offer high performance, efficiency, and code generation support. However, they may have protocol compatibility and setup complexity limitations.

Advantages:

- Polyglot: gRPC API's are polyglot which means that they can be used with a wide range of programming languages.
- Bidirectional: supports bidirectional streaming, real time communication between client and server.
- Fast & Strongly Typed.

Disadvantages:

- Complexity: more complex to setup and configure.
- Learning Curve: steeper learning curve.
- Limited Browser Support: well suited for server-server communication, lacks support for web browser.
