---
title: PowerDNS
authors:
  - name: Vinay Keshava
    link: https://blog.vinay.im/about
---

https://www.cloudflare.com/learning/dns/what-is-dns/
```
dig +trace amogha.labnetwork.in

; <<>> DiG 9.18.19-1~deb12u1-Debian <<>> +trace amogha.labnetwork.in
;; global options: +cmd
.			600	IN	NS	b.root-servers.net.
.			600	IN	NS	m.root-servers.net.
.			600	IN	NS	f.root-servers.net.
.			600	IN	NS	i.root-servers.net.
.			600	IN	NS	l.root-servers.net.
.			600	IN	NS	d.root-servers.net.
.			600	IN	NS	a.root-servers.net.
.			600	IN	NS	g.root-servers.net.
.			600	IN	NS	e.root-servers.net.
.			600	IN	NS	h.root-servers.net.
.			600	IN	NS	c.root-servers.net.
.			600	IN	NS	j.root-servers.net.
.			600	IN	NS	k.root-servers.net.
.			600	IN	RRSIG	NS 8 0 518400 20240130050000 20240117040000 30903 . 3UGfMmYAGBezyUTAir+TH1swje4FUz2dT6OkIbTzpcOHx/AUjndw/SKz y8BCTgaIltIwF0I6SOQYNe4vi22rVsfGGcVO+qTnTERKIUFS7VHP3cNw sCoXHvVASZMuhO2CLofz0YqnrEdmSG9jM2V/HYWEvTzmbPRZC2C+nz8a 8MuW666wyPH1Uum5iUPoz2Fouwfk0cT42mJ1X3I0exXazFkGeJGB7Sfv zP6irEH7WURZcLoDP3rY9DpIGCSsaKTy6bGvf8Muns4lPWSvFw92pFBr XrsKBCSNhr7wrBTOg1UM14P/c4ggUYLP35V4mp/K5RlKOfh3pd8+uoc2 B/nSww==
;; Received 1097 bytes from 127.0.0.1#53(127.0.0.1) in 0 ms

in.			172800	IN	NS	ns1.registry.in.
in.			172800	IN	NS	ns2.registry.in.
in.			172800	IN	NS	ns3.registry.in.
in.			172800	IN	NS	ns4.registry.in.
in.			172800	IN	NS	ns5.registry.in.
in.			172800	IN	NS	ns6.registry.in.
in.			86400	IN	DS	25291 8 2 2945B61339DFA7221AD82D5C6C7E3CE4E9C2523E7754ACC8131D0EF9 4617E2F2
in.			86400	IN	RRSIG	DS 8 1 86400 20240131050000 20240118040000 30903 . IFHCnVVRWAsLFEfioh6tQ377W9j6X0wMnhLYOf/jpwV165bfE3Eds37N vc2xebHces+WtwfNvMsczo6+qoLMEEQENNc3P5GHF0Tu0VwQ6KIKvbze I0NALAlv+J/r0FSAhNy1PoeR1vS8DLobaAjNCCy3qYGkpyKwlxqgusPC IU8BFCZaeEtz4XBeYNYn9lchY540xCGd7ZEcdI3S/OrTLblZQosJfc4l NyoYngvntdaDDdcuoRf3Usamrk3SUS3QUoOt/Qx3Z1LifU7QKbnnkjgW nGMFRgCqFnWCXjUotoPeQbNjthBL5E5SmWEvvYsNc2DMyUnqp9r/OX+h +zudAA==
;; Received 765 bytes from 199.7.91.13#53(d.root-servers.net) in 248 ms

labnetwork.in.		3600	IN	NS	ns6.tinydns.in.
labnetwork.in.		3600	IN	NS	ns5.tinydns.in.
u7smslveus494o8dr4h483un5spuc1tu.in. 900 IN NSEC3 1 1 0 - U7UB8S485RVUNK2H8F23BU6LRI1GJL00 NS SOA RRSIG DNSKEY NSEC3PARAM CDS CDNSKEY TYPE65534
u7smslveus494o8dr4h483un5spuc1tu.in. 900 IN RRSIG NSEC3 8 2 900 20240130195500 20240116192722 27000 in. wdxZl/4X8UG7iX4fMqIfYCPP8G9flaM7zgUCwrwlkqb7mtzyc7Vzvvyw IGqZgSrOz5VNtOxZd2u0+L+bGzbBhdLcUxPBzcKuIqs6pk1+etkyBIp5 IZbe35F6mkmO2pKrba1YPmLL7H7MYsudvuUcrbWWRg8MGb8sVFzJm49H eQvvK+3LXHV2SSv0ypLx1kKJuFwMQ+6MX02gEV7Uzdi74g==
e4tmotrmfrj6v3sii9g2rgv9j3r7i7sq.in. 900 IN NSEC3 1 1 0 - E4U5LD2VI0EOIE3ATE1MFHP5POUKAG26 NS DS RRSIG
e4tmotrmfrj6v3sii9g2rgv9j3r7i7sq.in. 900 IN RRSIG NSEC3 8 2 900 20240131185933 20240117185317 27000 in. haKYuAtWZP8zV+xf55vjJbWlIl/ZRVENv720IhDlPSLlPR1E6w0LDiqk E2yo/wKiIbHhmhL0I9cPpGXgBq7Ki8SmhBaU9nYqA2+y+s/u2AlcUF9I AAl2VZDtmLq7ty3dMrrJ2sSygx4SWWnH6hqRi+L8DqzbuwKRXE5PfgAF FI1dzezRn2kq9tJ+WILjtCAKPcccI8wAgHQlZXPUVOQklg==
;; Received 707 bytes from 156.154.101.20#53(ns6.registry.in) in 24 ms

amogha.labnetwork.in.	3600	IN	A	122.166.50.175
;; Received 65 bytes from 205.147.109.33#53(ns5.tinydns.in) in 40 ms

```


- powerdns and systemd resolved both uses port 53 to resolve dns queries.
```
root@powerdns:/etc/powerdns# netstat -tnpl
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:5355            0.0.0.0:*               LISTEN      282/systemd-resolve
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      282/systemd-resolve
tcp        0      0 10.22.13.150:8082       0.0.0.0:*               LISTEN      1633/pdns_recursor
tcp        0      0 127.0.0.54:53           0.0.0.0:*               LISTEN      282/systemd-resolve
tcp        0      0 0.0.0.0:25              0.0.0.0:*               LISTEN      698/master
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      419/sshd: /usr/sbin
tcp        0      0 127.0.0.1:53            0.0.0.0:*               LISTEN      1633/pdns_recursor
tcp6       0      0 :::5355                 :::*                    LISTEN      282/systemd-resolve
tcp6       0      0 :::25                   :::*                    LISTEN      698/master
tcp6       0      0 :::22                   :::*                    LISTEN      419/sshd: /usr/sbin
```

```
root@powerdns:/etc/powerdns# systemctl stop systemd-resolved
root@powerdns:/etc/powerdns# netstat -tnpl
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 10.22.13.150:8082       0.0.0.0:*               LISTEN      1633/pdns_recursor
tcp        0      0 0.0.0.0:25              0.0.0.0:*               LISTEN      698/master
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      419/sshd: /usr/sbin
tcp        0      0 127.0.0.1:53            0.0.0.0:*               LISTEN      1633/pdns_recursor
tcp6       0      0 :::25                   :::*                    LISTEN      698/master
tcp6       0      0 :::22                   :::*                    LISTEN      419/sshd: /usr/sbin
root@powerdns:/etc/powerdns#
```

ss -anp | grep 53


https://www.howtogeek.com/are-smart-plugs-and-power-points-the-best-smart-home-upgrade/
What the Hell is loopback address like 127.0.0.1
