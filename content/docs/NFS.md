---
title: NFS-Network File System
authors:
  - name: Vinay Keshava
    link: https://blog.vinay.im/about
---

NFS stands for Network FIle System, is a file system protocol that allows to access files and folders on a remote system,and accessing them like a local storage.The architecture of NFS is client-server architecture,where the files are folders to be accessed are stored on the servers and these file & folders are accessed by the client machine like a mounted partition.

With NFS,its easy to access file,enable special permissions like either read only or read and write,syncing the files and folders across the available clients.

### Install NFS Server

I'm currently on Debian Bookworm, to install the nfs server package just do ```sudo apt install nfs-kernel-server```
```
$ cat /etc/issue
Debian GNU/Linux 12 \n \l

$ sudo apt policy nfs-kernel-server
nfs-kernel-server:
  Installed: 1:2.6.2-4
  Candidate: 1:2.6.2-4
  Version table:
 *** 1:2.6.2-4 500
        500 mirror+file:/etc/apt/mirrors/debian.list bookworm/main amd64 Packages
        100 /var/lib/dpkg/status
```

Create a export folder/directory where files and folders are accessed by the clients present on the server, here i have choosed the location ```/mnt/sharedfolder``` 

```
$ sudo mkdir -p /mnt/sharedfolder
```

change the permissions of the folder to ```777```

```sudo chmod 777 /mnt/sharedfolder```

It will allow the clients to access the shared folder.

### Configure Export Directory

The configuration file for the NFS server is located at ```/etc/exports```,here can specify directories that are to be shared to clients

```bash
/mnt/sharedfolder 10.22.13.0/24(rw,sync,no_subtree_check)
```

add the above line to ```/etc/exports```, in the form ```directory hostname(options)

Here ```/mnt/sharedfolder``` is the remote folder, ```10.22.13.0/24``` is the subnet IP where the folder can be accessed across the whole subnet network,this can also be filtered to access from specifc client IP too, and example would be

```bash
/mnt/sharedfolder 10.22.13.101(rw,sync,no_subtree_check)
```
where ```10.22.13.101``` is the client IP, to which only this IP can access the shared folder,no other IP can access.

- There are plenty of options to configure the NFS server:
	- ```ro``` : directory mounted as read only.
	- ```rw``` : directory mounted with read and write permissions.
	- ```subtree_check``` – specifies that, in the case of a directory is exported instead of an entire filesystem, the host should verify the location of files and directories on the host filesystem
	- ```no_subtree_check``` – specifies that the host should not check the location of the files being accessed within the host filesystem
	- ```sync``` – this just ensures that the host keeps any changes uploaded to the shared directory in sync
	- ```async``` – ignores synchronization checks in favor of increased speed

Exporting the shared directory

After the ```/etc/exports``` nfs server is configured,to export the shared directory run the below command and restart nfs-kernel-server
```bash
$ sudo exportfs -a
$ sudo systemctl restart nfs-kernel-server
```

Configure the firewall so that the server is open for clients to access the shared content.

```bash
$ sudo ufw allow from 10.22.13.0/24 to any port nfs
```
and again check ufw status
```
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
22/tcp                     ALLOW       Anywhere                  
2049                       ALLOW       10.22.13.0/24             
22/tcp (v6)                ALLOW       Anywhere (v6)
```

### Configuring Client

Once the server and firewall is setup,next thing is to configure the client

To configure the client on Debian install the package ```nfs-common```

```bash
$ sudo apt install nfs-common
```

Create a mount point on the client machine for the NFS server's shared folder

```bash{filename="On client machine"}
$ sudo mkdir -p /mnt/sharedfolder_client
```

Next step is to mount the remote folder on the client,this can be done in two ways,with the ```mount``` command or adding the entry to ```/etc/fstab```
- Using the mount command
```bash{filename="On client"}
$ sudo mount 10.22.13.53:/mnt/sharedfolder /mnt/sharedfolder_client
```

where ```10.22.13.53``` is the local ip of the NFS server,basically mounting ```/mnt/sharedfolder``` of the client to ```/mnt/sharedfolder_client```.

- Using the ```/etc/fstab``` way
```
$ cat /etc/fstab
# /etc/fstab: static file system information
UUID=8f994d1d-8ae8-4dff-aaa5-9aef4574329b / ext4 rw,discard,errors=remount-ro,x-systemd.growfs 0 1
UUID=5F3D-98F1 /boot/efi vfat defaults 0 0

10.22.13.53:/mnt/sharedfolder /mnt/sharedfolder_client nfs4  defaults,user,exec  0 0
```
where ```/mnt/sharedfolder_client``` is the mountpoint on the client,editing any file from the client update's it on the server,because we have configured to sync across all the mount points,editing any file from the client will update it on the NFS server too.


