---
type: docs
title: "Ant"
---

Apache Ant, is Java based make tool, which process according to the build commands given in `build.xml`. Ant is mainly used for Java applications, which can be used to run,compile and package java software.
Configuration for the build is done in `build.xml` file.
Software projects looking for build tool and dependency management can use Ant.

Simple Hello World project enabling `Ant` build system for the Hello World project.
Pre-requisties are to install openjdk and ant build system.

```java{filename="HelloWorld.java"}
$ cat src/HelloWorld.java 
public class HelloWorld{
	public static void main(String []args){
		System.out.println("Hello World");
		System.out.println("Vinay Keshava");
   }
	}
```
The conventional way to run the above simple HelloWorld.java is using `javac` and `java`.

```bash
$ javac HelloWorld.java

$ java HelloWorld
Hello World
Vinay Keshava
```

In order to simplify the processing of building and running the java code, we use build systems like Apache Ant,Maven and Gradle.
Here lets create a simple `build.xml` file for the above HelloWorld.java using Ant build system.

```bash
$ tree
.
├── build.xml
└── src
    └── HelloWorld.java

1 directory, 2 files
```
Create a project where the source within the `src` directory and `build.xml` within the root directory having the contents.

```xml{filename=build.xml}
<project>
    <target name="clean">
        <delete dir="build"/>
    </target>
    <target name="compile">
        <mkdir dir="build/classes"/>
        <javac srcdir="src" destdir="build/classes"/>
    </target>
    <target name="jar">
        <mkdir dir="build/jar"/>
        <jar destfile="build/jar/HelloWorld.jar" basedir="build/classes">
            <manifest>
                <attribute name="Main-Class" value="HelloWorld"/>
            </manifest>
        </jar>
    </target>
    <target name="run">
        <java jar="build/jar/HelloWorld.jar" fork="true"/>
    </target>
</project>
```
A simple xml file having tags like targets where the operations are defined, here we have three target types `compile`,`jar`,`run`.
In `compile` phase a directory called `build/classes` is created, and destination directory is set.

```java
vinay@falcon:/tmp/ant-sample$ ant compile
Buildfile: /tmp/ant-sample/build.xml

compile:
    [mkdir] Created dir: /tmp/ant-sample/build/classes
    [javac] /tmp/ant-sample/build.xml:9: warning: 'includeantruntime' was not set, 
	defaulting to build.sysclasspath=last; set to false for repeatable builds
    [javac] Compiling 1 source file to /tmp/ant-sample/build/classes

BUILD SUCCESSFUL
Total time: 0 seconds
vinay@falcon:/tmp/ant-sample$ ant jar
Buildfile: /tmp/ant-sample/build.xml

jar:
    [mkdir] Created dir: /tmp/ant-sample/build/jar
      [jar] Building jar: /tmp/ant-sample/build/jar/HelloWorld.jar

BUILD SUCCESSFUL
Total time: 0 seconds
vinay@falcon:/tmp/ant-sample$ ant run
Buildfile: /tmp/ant-sample/build.xml

run:
     [java] Hello World
     [java] Vinay Keshava

BUILD SUCCESSFUL
Total time: 0 seconds
```

All the above three commands can be run simultaneously, `ant compile jar run` where the code is compiled,packaged and run.

- Compile : a directory `build/classes` is created, source directory is set with javac command tag.

- Jar : creating a directory `build/jar`,where the jar is stored.

- Run : running the HelloWorld.jar file.

