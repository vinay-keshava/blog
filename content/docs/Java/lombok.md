---
type: docs
title: "Lombok"
---

Lombok was created to avoid boilerplate code,Replaces code with java annotations

    - `@AllArgsConstructor`: Generates an all arguments AllArgsConstructor
    - `@Builder`:   Generates class instances using builder API,we can build all the entire POJO    using the parameters
    - `@Data`: is like having implicit @Getter, @Setter, @ToString, @EqualsAndHashCode and @RequiredArgsConstructor
    - `@Getter`: generates getter methods for the attributes
    - `@Setter`: generates setter methods for the attributes
    - `@NoArgsConstructor`: creates a constructor with no arguments
    - `@RequiredArgsConstructor`: creates a constructor with all final field attributes
