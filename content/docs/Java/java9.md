---
type: docs
title: "Java 9"
---

### Java Modules

JPMS(Java Platform Module System),also known as Project Jigsaw,helps us in encapsulating packages and managing dependencies.
    - Class is a container of fields and methods
    - Package is a container of classes and interfaces
    - module is a container of packages

The module system helps to provide a secure and structured approach to writing java code by better organizing components,preventing from using malicious code.

Benefits of Java module

    - Strong Encapsulatin: modules allows to encapsulate code,and hide its implementation details.Reduce the risk of coupling and improve maintainability of code
    - Better Organization: Modules helps you to organize code into logical units,making it easier to navigate and group related classes and packages together in a module.
    - Improved Security: Controll access to your code,can specify which group of classes and packages to be exported from the module.
