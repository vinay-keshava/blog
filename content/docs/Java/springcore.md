---
type: docs
title: "Spring Core Framework"
---

Spring is a popular framework in Java Enterprise Edition.Spring can be thought of as a framework of frameworks,because it provides support to other frameworks as well such as Hibernate,JPA etc

Some of the key and core features of spring core are as belows:

### Dependency Injection

Dependency Injection provides Objects which an object needs.Dependency injection generally means passing a dependent object as a parameter to a method, rather than having the method create the dependent object.Instead of making the objects tightly coupled on other objects, an initiative to make objects loosely coupled.

Conventionally,we create objects using the `new` operator.

```
class Employee { 
   private int id;
   private String name;
   private Address address;
 }
}

class Address {
   private String street;
   private String city;
 }
}
```

In this case `Address` class values are only set when the class `Employee` is instantiated,making it a tight coupling, spring resolves this type of tight coupling with the help of Dependency Injection in two ways, `Setter Injection` and `Constructor Injection`.This is achieved in spring by IOC.

In spring dependency injection is of two types, XML based and annotaiton based.

### Inversion of Control

Spring IoC container is the core of spring framework.It creates objects,configures and assemble their dependencies,manage entire life cycle of each object.

  - Information about the object's configuration is done by the configuration file(XML),java annotations or java code,these objects are called `Beans`.

  - Spring IoC internally uses Dependency Injection to manage object life cycle.
  - Since the control of these objects are not in the hands of developers hence the name,`Inversion of Control`.
  - There are two types of IoC containers in Spring
      1. Bean Factory
      2. Application Context
    
  - Bean Factory is the basic version of IoC container,while ApplicationContext extends the features of BeanFactory.
  - Some of the main features of IoC containers.
    1. Creating Objects
    2. Managing dependencies
    3. Application Configuration
    4. Managing Objects

### Constructor Injection & Setter Injection

In constructor injection dependencies are passed as parameter to the class's constructor.In this way all the dependencie's objects are created during the objection creation,say for example there is a class called `Employee` with another object `Address` as its dependency,along with other attributes.
The `Address` class object is created whenever the `Employee` class object is instantiated.

`@Autowired` annotation uses constructor Injection.

In Setter Injection dependencies are injected with the help of getters and setters 


### Autowiring

Autowiring in spring framework is used to automatically inject dependencies.Spring IoC container detects the dependencies mentioned in the cofiguration file and the realtionship between the beans,this is known as autowiring.
`@Autowired` annotation is used,autowiring in spring internally uses constructor injection.


### Common Annotations 

1. `@Configuration`: Used to declare a class with one or more `@Bean` methods.These classes are processed by the spring container to generate bean definitions.

2. `@Bean`: Indicates that a method produces a bean to be managed by the Spring container.

3. `@ComponentScan` : is used along with @Configuration annotation to specify the packages that we want to be scanned.If no arguments it scans the current package and all of its sub packages.

4. `@Component` : is a class level annotation,used to denote a class as component.This annotation is used to mark the beans as Spring managed components.
    1. @Service - Annotated class is a service,specialization of `@Component`, used for business logic or services in it.
    2. @Repository - dealing with CRUD operations,mostly deals with DAO or repositories that deal with databases.
    3. @Controller - front controllers,responsible to handle user requests and for returning appropriate responses to user.

5. `@Autowired` : is used for automatic injection of beans.

### Spring MVC Annotations

1. `@Controller` or `@RestController` components use annotation to express request mappings,result input,exception handling and more.

2. `@RequestMapping` : class level or method level annotation in controller,class level annotation maps a specific request path onto a controller,used to handle web requests,

3. `@PathVariable` : handle template variables in request URI mapping.

4. `@RequestParam` : to read form data and bind it automatically to the parameter present in the provided method.

5. `@RequestBody` : RequestBody is mainly used with CRUD operations to read request body.

6. `@ResponseBody` : is used with GET methods  to write the response body content.


### Spring Annotations.

1. `@SpringBootApplication` : Many spring boot developers like their apps to use Auto Configuration,component scan and be able define extra configuration on their application class.

        - @EnableAutoConfiguration: enables spring boot's auto configuration
        - @ComponentScan : enable component scan on the package where the application is located.
        - @Configuration : allow to register extra beans

2. `@EnableAutoConfiguration` : attempts to automatically configure your spring application based on the dependencies.


### Spring Data JPA Annotations

1. `@Transactional`: class or method level annotation,`Propagation Type`  of the transaction, `Isolation Level` of the transaction,`Timeout` for the transaction,`Rollback` rules for transactions.

2. `@Id` : marks field in model class as primary key.

3. `@Query`: Native SQL queries & JPQL implementation.

4. `@Lock` : We can configure Lock Mode when we execute a repository query method.
            Available lock modes are :
            - READ
            - WRITE
            - OPTIMISTIC
            - OPTIMISTIC_FORCE_INCREMENT
            - PESSIMISTIC_READ
            - PESSIMISTIC_WRITE
            - PESSIMISTIC_FORCE_INCREMENT
            - NONE

