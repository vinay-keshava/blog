---
title: Logical Volume Manager
authors:
  - name: Vinay Keshava
    link: https://blog.vinay.im/about
---

LVM is a disk management tool, which is a device mapper for linux kernel,managing storage systems than the traditional partition based one.In LVM instead of creating partitions you create 
logical volumes, ```/boot``` cannot use logical volumes.

LVM offers features like striping,mirroring,snapshotting.LVM functions by layering abstractions on top of physical storage devices.

Main components of LVM are 

- ```Physical Volumes```: physical block devices which are set a hard disks,physical volumes are regular storage devices,prefixed with ```pv..``` 
- ```Volume Groups```: LVM combines physical volumes into storage pools known as volume groups,LVM abstracts the function there by making a unified logical devices with one ,prefixed with ```vg...```
- ```Logical Volumes```: a volume group is sliced into a number of logical volumes.Logical volumes are functionally equivalent to partitions on a physical disks,but with more advanced features.

### Basic LVM Commands 
#### Making Physical Devices as Physical Volumes.

- lvmdiskscan - returns the number of block devices to which LVM can interact.
```
vinay@kevin ~> sudo lvmdiskscan
  /dev/sda       [      57.30 GiB] 
  /dev/nvme0n1p1 [    <464.81 GiB] 
  /dev/nvme0n1p5 [     975.00 MiB] 
  /dev/sdb       [     <28.87 GiB] 
  2 disks
  2 partitions
  0 LVM physical volume whole disks
  0 LVM physical volumes
```
Here we are creating a physical volume for the disks ```/dev/sda and /dev/sdb```.

- pvcreate - command that is used to initialize physical disks to physical volumes for LVM, this will write an LVM header to the devices to indicate that they are ready to be added to volume group.
```
vinay@kevin ~> sudo pvcreate /dev/sda /dev/sdb
  Physical volume "/dev/sda" successfully created.
  Physical volume "/dev/sdb" successfully created.
```
- pvs - displays information about physical volumes.

```
vinay@kevin ~> sudo pvs
  PV         VG Fmt  Attr PSize   PFree  
  /dev/sda      lvm2 ---   57.30g  57.30g
  /dev/sdb      lvm2 ---  <28.87g <28.87g
```
the disks ```/dev/sda``` and ```/dev/sdb``` is of the size 57G and 28G respectively,hence these can be added to the volume groups.


#### Add Physical Volumes to Volume Group.
- vgcreate - creates a volume group
```
vinay@kevin ~> sudo vgcreate VolGrpLvm /dev/sda /dev/sdb
  Volume group "VolGrpLvm" successfully created
```
a volume group with the name, ```VolGrpLvm``` is created 

- vgs - displays information about volume groups
 
```
vinay@kevin ~> sudo vgs
  VG        #PV #LV #SN Attr   VSize  VFree 
  VolGrpLvm   2   0   0 wz--n- 86.16g 86.16g
```
Here the two phyical volumes are converted into a pool of storage,making a total of 86G, from this pool of storage,these are sliced to several logical volumes.

#### Creating Logical Volumes from the Volume Group Pool.

- lvcreate - creating a logical volume from the volume group

```
vinay@kevin ~> sudo lvcreate -L 10G -n backup VolGrpLvm
  Logical volume "backup" created.
vinay@kevin ~> sudo lvs
  LV     VG        Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup VolGrpLvm -wi-a----- 10.00g
```

```lvcreate``` is used to create a logical volume from the volume group of storage pool, ```-L``` option is used to specify the size of the volume and ```-n``` option is used to label the logical volume.
 
```lvs``` displays information about the logical volume created.

Similarly create another logical volume.
```
vinay@kevin ~> sudo lvcreate -L 30G -n college-data VolGrpLvm
  Logical volume "college-data" created.
vinay@kevin ~> sudo lvs
  LV           VG        Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup       VolGrpLvm -wi-a----- 10.00g                                                    
  college-data VolGrpLvm -wi-a----- 30.00g   
```

#### Format and Mount the logical volumes

Formatting the logical volumes.
```
$ sudo mkfs.ext4 /dev/VolGrpLvm/backup
$ sudo mkfs.ext4 /dev/VolGrpLvm/college-data
```

Mounting the logical volume

```
vinay@kevin ~> sudo mkdir -p /mnt/{backup,college-data}
vinay@kevin ~> sudo mount /dev/VolGrpLvm/backup /mnt/backup/
vinay@kevin ~> sudo mount /dev/VolGrpLvm/college-data /mnt/college-data/
vinay@kevin ~> lsblk
NAME                      MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda                         8:0    1  57.3G  0 disk 
├─VolGrpLvm-backup        254:0    0    10G  0 lvm  /mnt/backup
└─VolGrpLvm-college--data 254:1    0    30G  0 lvm  /mnt/college-data
sdb                         8:16   1  28.9G  0 disk 
nvme0n1                   259:0    0 465.8G  0 disk 
├─nvme0n1p1               259:1    0 464.8G  0 part /
├─nvme0n1p2               259:2    0     1K  0 part 
└─nvme0n1p5               259:3    0   975M  0 part [SWAP]
```
Creating mount points in ```/mnt``` and mount the volumes onto the mountpoints created.

{{% details title=" Other useful scan and display commands." closed="true" %}}

```
vinay@kevin ~> sudo pvscan 
  PV /dev/sda   VG VolGrpLvm       lvm2 [57.30 GiB / 17.30 GiB free]
  PV /dev/sdb   VG VolGrpLvm       lvm2 [28.86 GiB / 28.86 GiB free]
  Total: 2 [86.16 GiB] / in use: 2 [86.16 GiB] / in no VG: 0 [0   ]
vinay@kevin ~> sudo lvscan
  ACTIVE            '/dev/VolGrpLvm/backup' [10.00 GiB] inherit
  ACTIVE            '/dev/VolGrpLvm/college-data' [30.00 GiB] inherit
vinay@kevin ~> sudo vgscan
  Found volume group "VolGrpLvm" using metadata type lvm2
vinay@kevin ~> s^C
vinay@kevin ~> 
vinay@kevin ~> sudo pvscan 
  PV /dev/sda   VG VolGrpLvm       lvm2 [57.30 GiB / 17.30 GiB free]
  PV /dev/sdb   VG VolGrpLvm       lvm2 [28.86 GiB / 28.86 GiB free]
  Total: 2 [86.16 GiB] / in use: 2 [86.16 GiB] / in no VG: 0 [0   ]
vinay@kevin ~> sudo vgscan
  Found volume group "VolGrpLvm" using metadata type lvm2
vinay@kevin ~> sudo lvscan
  ACTIVE            '/dev/VolGrpLvm/backup' [10.00 GiB] inherit
  ACTIVE            '/dev/VolGrpLvm/college-data' [30.00 GiB] inherit
vinay@kevin ~> sudo pvdisplay 
  --- Physical volume ---
  PV Name               /dev/sda
  VG Name               VolGrpLvm
  PV Size               57.30 GiB / not usable 4.00 MiB
  Allocatable           yes 
  PE Size               4.00 MiB
  Total PE              14669
  Free PE               4429
  Allocated PE          10240
  PV UUID               obDX3S-49P0-0Y2o-Vpr1-zFxV-Tgng-Ov9oEz
   
  --- Physical volume ---
  PV Name               /dev/sdb
  VG Name               VolGrpLvm
  PV Size               <28.87 GiB / not usable 4.00 MiB
  Allocatable           yes 
  PE Size               4.00 MiB
  Total PE              7389
  Free PE               7389
  Allocated PE          0
  PV UUID               7KAaVc-1H9V-FSsK-n98X-P9SM-IIAw-s9Myce
   
vinay@kevin ~> sudo vgdisplay 
  --- Volume group ---
  VG Name               VolGrpLvm
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               86.16 GiB
  PE Size               4.00 MiB
  Total PE              22058
  Alloc PE / Size       10240 / 40.00 GiB
  Free  PE / Size       11818 / 46.16 GiB
  VG UUID               P1dIv1-Lk34-Oxjo-ZZoE-81ys-3XnL-5WZotd
   
vinay@kevin ~> sudo lvdisplay 
  --- Logical volume ---
  LV Path                /dev/VolGrpLvm/backup
  LV Name                backup
  VG Name                VolGrpLvm
  LV UUID                CjtnRq-evig-hsY4-avOM-6KVM-a7TT-VeAkDR
  LV Write Access        read/write
  LV Creation host, time kevin, 2024-01-03 10:41:55 +0530
  LV Status              available
  # open                 1
  LV Size                10.00 GiB
  Current LE             2560
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           254:0
   
  --- Logical volume ---
  LV Path                /dev/VolGrpLvm/college-data
  LV Name                college-data
  VG Name                VolGrpLvm
  LV UUID                f2zixQ-ayec-zrXb-AV35-JaU0-9jz5-9ZrFtk
  LV Write Access        read/write
  LV Creation host, time kevin, 2024-01-03 10:45:39 +0530
  LV Status              available
  # open                 1
  LV Size                30.00 GiB
  Current LE             7680
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           254:1
```
{{% /details %}}

#### Rename a LV
Renaming a logical volume using lvrename
```
vinay@kevin ~> sudo lvrename /dev/VolGrpLvm/college-data /dev/VolGrpLvm/college
  Renamed "college-data" to "college" in volume group "VolGrpLvm"
vinay@kevin ~> ls /dev/VolGrpLvm/
backup@  college@
```

#### Extending a LV

To increase the size of the logical volume,we use ```lvextend``` command. 

```
vinay@kevin ~> sudo lvs
  LV      VG        Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup  VolGrpLvm -wi-ao---- 10.00g                                                    
  college VolGrpLvm -wi-ao---- 30.00g                                                    
vinay@kevin ~> sudo lvextend -L+5G /dev/VolGrpLvm/backup 
  Size of logical volume VolGrpLvm/backup changed from 10.00 GiB (2560 extents) to 15.00 GiB (3840 extents).
  Logical volume VolGrpLvm/backup successfully resized.
```
To increase the size of the volume by specified size use the ```+``` character to mention the increase in size. 

To set the size of the volume see the below example.
```
vinay@kevin ~> sudo lvextend -L25G /dev/VolGrpLvm/backup
  Size of logical volume VolGrpLvm/backup changed from 15.00 GiB (3840 extents) to 25.00 GiB (6400 extents).
  Logical volume VolGrpLvm/backup successfully resized.
vinay@kevin ~> sudo lvs
  LV      VG        Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup  VolGrpLvm -wi-ao---- 25.00g                                                    
  college VolGrpLvm -wi-ao---- 30.00g
  ```

#### Shrink a LV

Can reduce the size of the volume using ```lvreduce``` command.
```
vinay@kevin ~> sudo lvs
  LV      VG        Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup  VolGrpLvm -wi-ao---- 25.00g                                                    
  college VolGrpLvm -wi-ao---- 30.00g                                                    
vinay@kevin ~> sudo lvreduce  -L20G /dev/VolGrpLvm/backup 
  WARNING: Reducing active and open logical volume to 20.00 GiB.
  THIS MAY DESTROY YOUR DATA (filesystem etc.)
Do you really want to reduce VolGrpLvm/backup? [y/n]: y
  Size of logical volume VolGrpLvm/backup changed from 25.00 GiB (6400 extents) to 20.00 GiB (5120 extents).
  Logical volume VolGrpLvm/backup successfully resized.
vinay@kevin ~> sudo lvs
  LV      VG        Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup  VolGrpLvm -wi-ao---- 20.00g                                                    
  college VolGrpLvm -wi-ao---- 30.00g
  ```
  Here,the size of the volume is reduced to 20G.

#### Remove an LV

Before removing an LV,umount it first and then use the ```lvremove``` to remove the volume group.
```
vinay@kevin ~> sudo lvremove /dev/VolGrpLvm/backup
Do you really want to remove active logical volume VolGrpLvm/backup? [y/n]: y
  Logical volume "backup" successfully removed.
```
#### Increase Pool storage

To increase the size of the volume group we use the command ```vgextend```  to increase the size of the storage pool.

Initially the size of the volume storage pool was ```86G``` and now it is increased to ```115G```
```/dev/sdc``` is the new phyical volume.

```
vinay@kevin ~> sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
vinay@kevin ~> sudo vgs
  VG        #PV #LV #SN Attr   VSize  VFree 
  VolGrpLvm   2   1   0 wz--n- 86.16g 56.16g
vinay@kevin ~> sudo vgextend VolGrpLvm /dev/sdc
  Volume group "VolGrpLvm" successfully extended
vinay@kevin ~> sudo vgs
  VG        #PV #LV #SN Attr   VSize    VFree  
  VolGrpLvm   3   1   0 wz--n- <115.03g <85.03g
```

### Striping

#### RAID 0: Striping
RAID 0,also known as striped set or striped volume,requires a minimum of two disks, the disks are merged into one single large volume and data is stored evenly the disks.This process is 
called disk striping and involves splitting data into blocks and writing it simultaneously/sequentially on multiple disks.
It doesn't support redundancy,fault tolerance and hence data loss.

Data is stored sequentially

```
vinay@kevin ~> sudo lvcreate -L 10G -i2 -I64 -n backup1 VolGrpLvm
[sudo] password for vinay: 
WARNING: ext4 signature detected on /dev/VolGrpLvm/backup1 at offset 1080. Wipe it? [y/n]: y
  Wiping ext4 signature on /dev/VolGrpLvm/backup1.
  Logical volume "backup1" created.
```
Creating a logical volume with the size ```10G```, the ```-i``` argument is used to mention striping and the number next to the option specifies the striping in how many disks, the ```-I``` option in the above command is to specify the size of block of 64kB across two physical volumes.

If the striping number across the disks is more than the physical volumes,it fails to create a logical volume,with an exeception saying ```number of stripes exceeds number of physical volumes```.

```
vinay@kevin ~> sudo lvcreate -L 5G -i4 -I64 -n backup3 VolGrpLvm
  Number of stripes (4) must not exceed number of physical volumes (3)
```
In this setup only 3 physical volumes are used,which exceeds the stripe number.


### Mirroring

RAID 1: Disk Mirroring
Keeping a copy of the data within the logical volume is called mirroring,for redundancy and to avoid data loss.

the ```-m``` option with value ```1``` creates one copy of the data,therefore having a total of two copies of data within the file system,```-m2``` creates two mirrors,yielding three copies of the file system.
```
vinay@kevin ~> sudo lvcreate -L 5G -m1 -n mirrorLV VolGrpLvm
  Logical volume "mirrorLV" created.
```

### Thin Volumes
Usually LVM normally allocates blocks when you create a volume,thats thick provisioning.LVM thin pools instead allocates blocks when they are written,this is called thin provisioning.
Because volumes can be much larger than physically available space.

- To remove a physical volume from the existing volume group,use vgreduce
```
vinay@kevin ~ [5]> sudo vgreduce VolGrpLvm /dev/sdc
  Removed "/dev/sdc" from volume group "VolGrpLvm"
```

Create new physical volumes using physical disks and create a volume group.
```
vinay@kevin ~> sudo pvcreate /dev/sdc /dev/sdd
  Physical volume "/dev/sdc" successfully created.
  Physical volume "/dev/sdd" successfully created.
vinay@kevin ~> sudo vgcreate vg_thin /dev/sdc /dev/sdd
  Volume group "vg_thin" successfully created
```

Create a thinpool with the size 5G with chunksize 256K where ```vg_thin`` is the volume group name and creating a pool again within the volume group
```
vinay@kevin ~> sudo lvcreate --thin --size 5G --chunksize 256K --poolmetadatasize 1G vg_thin/thin_pool
  Thin pool volume with chunk size 256.00 KiB can address at most 63.50 TiB of data.
  Logical volume "thin_pool" created.
```
Listing all the logical volumes which displays all the logical volumes 
```
vinay@kevin ~> sudo lvs -a
  LV                VG      Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  [lvol0_pmspare]   vg_thin ewi------- 1.00g                                                    
  thin_pool         vg_thin twi-a-tz-- 5.00g             0.00   1.57                            
  [thin_pool_tdata] vg_thin Twi-ao---- 5.00g                                                    
  [thin_pool_tmeta] vg_thin ewi-ao---- 1.00g                                                    
```

Creating 3 logical volumes,while for the second logical volujme ```volume_2``` there is a warning which mentions exceeds the size of thin pool,even though the pool size is 5G,after the creation of the first logical volume ```volume_1``` 2G should be remained,since it is thin provisioning, only when data is written blocks are allocated to it,so hence just a warning.
Similarly the error comes during the creation of ```volume_3```.
```
vinay@kevin ~> sudo lvcreate --thinpool vg_thin/thin_pool --name volume_1 --virtualsize 3G
  Logical volume "volume_1" created.

vinay@kevin ~> sudo lvcreate --thinpool vg_thin/thin_pool --name volume_2 --virtualsize 3G
  WARNING: Sum of all thin volume sizes (6.00 GiB) exceeds the size of thin pool vg_thin/thin_pool (5.00 GiB).
  WARNING: You have not turned on protection against thin pools running out of space.
  WARNING: Set activation/thin_pool_autoextend_threshold below 100 to trigger automatic extension of thin pools before they get full.
  Logical volume "volume_2" created.

vinay@kevin ~> sudo lvcreate --thinpool vg_thin/thin_pool --name volume_3 --virtualsize 3G
  WARNING: Sum of all thin volume sizes (9.00 GiB) exceeds the size of thin pool vg_thin/thin_pool (5.00 GiB).
  WARNING: You have not turned on protection against thin pools running out of space.
  WARNING: Set activation/thin_pool_autoextend_threshold below 100 to trigger automatic extension of thin pools before they get full.
  Logical volume "volume_3" created.
```

All the logical volumes can be verfied now.
```
vinay@kevin ~> sudo lvs -a
  LV                VG      Attr       LSize Pool      Origin Data%  Meta%  Move Log Cpy%Sync Convert
  [lvol0_pmspare]   vg_thin ewi------- 1.00g                                                         
  thin_pool         vg_thin twi-aotz-- 5.00g                  0.00   1.57                            
  [thin_pool_tdata] vg_thin Twi-ao---- 5.00g                                                         
  [thin_pool_tmeta] vg_thin ewi-ao---- 1.00g                                                         
  volume_1          vg_thin Vwi-a-tz-- 3.00g thin_pool        0.00                                   
  volume_2          vg_thin Vwi-a-tz-- 3.00g thin_pool        0.00                                   
  volume_3          vg_thin Vwi-a-tz-- 3.00g thin_pool        0.00
```

