---
title: ListMonk
authors:
  - name: Vinay Keshava
    link: https://blog.vinay.im/about
---

[ListMonk](https://listmonk.app) is a selfhosted one way mailing list and newsletter for organizations.
It doesn't have an inbuilt SMTP server, but can configure to use any exisiting SMTP server to send emails from the mailing list.

### Installation

Listmonk is a simple single binary with only dependency being postgresql, can also host it using docker if you want to set it up in a isolated environment.

```bash
# Download the compose file to the current directory.
curl -LO https://github.com/knadh/listmonk/raw/master/docker-compose.yml

# Run the services in the background.
docker compose up -d
```
Then, visit http://localhost:9000 to create the Super Admin user and login.
Configure `docker-compose.yml` according to your needs and changes. 
For caddy webserver configuration  here is the simple config `Caddyfile`

```bash
https://lists.example.in {
	reverse_proxy localhost:9000
}
```
Below is the screenshot of the Listmonk Newsletter and mailing list.
<img src="/images/listmonk_1.png" alt="ListMonk Dashboard" style="width:850px;"/>

### SMTP Server Configuration

You can use any email provider with listmonk to send emails. To configure listmonk with gmail, input `host` as `smtp.gmail.com` with port number `587`.

Enable 2 Factor Authentication on your gmail account settings, and  create an app password.
Input the email address and the app password you created in the listmonk SMTP settings.

To test the connection, send a test email to another email address.

![image info](/images/listmonk_2.png)

### Mailing Lists

Add a new subscriber by clicking on the `+ New` button, and assign subscribers to mailing lists.
Create a new campaign, select multiple mailing list address of different categories to send the email campaigns.

### Key Features of Listmonk

- Import and Export option of subscribers.
- Easy to self host within minutes and maintain
- Simple dashboard to display statistics.
- Configuring Media Uploads.
- Bounces settings.
- hCaptcha Integration.
- Campaigns to send emails
- Single Opt-in and Double Opt-in Subscribers.
- Unsubscribe settings.
- JSON field to add extra information about subscribers.
- Privacy tab under settings.
- Messengers and Custom CSS for appearance.
