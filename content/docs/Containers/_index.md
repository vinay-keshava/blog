---
title: Containers 101
type: docs
draft: false
sidebar:
  open: false
---

Container have existed within operating systems for quite a long time, `A Container is a runtime intiative of container image`.

Linux containers are often isolated through the use of cgroups,SELinux,AppArmor.

`cgroups` - is a linux kernel feature that limits,accounts and isolates the resource usage(CPU,memory,disk I/O,etc ) of collection of processes, this allocates resources.Resource Limitations,Prioritization,Accounting,Control are few features of cgroups.

`AppArmor` - is a mandatory access control framework,when enabled it defines set of rules that specify what files a given program can access.AppArmor is a linux application security system, protects the operating system and applications from external and internal threats.

### Container Teminology

Like a normal linux program,containers have two states - rest and running. When the container is at rest, a container is a like a file that is stored on the disk like other files.This is referred to as `Container Image` or `Container Repository`. When the container is started, the `Container Engine` unpacks the required files and meta-data, then hands them to the linux kernel.The next process is to start the container just like another linux process making an API call to the linux kernel. This API call makes an isolation and mounts  a copy of the files stored in the container image.

Few `Container Engines` are: Docker,CRI-O, LXC. These container engines takes a container image and turn them into container.How the container is started is defined in Container Runtime Specification and Reference Runtime Implementation called `runc`, goverened by OCI [Open Container Initiative](https://opencontainers.org/)

#### Registry Server

A container registry server is a tool which hosts and distributes container images. The container registry server can either be public or private.These container registries connect directly to container orchestration platforms like Docker and Kubernetes. 

You need to trust the registry servers of container first,the docker daemons in Debian is configured to pull images by default from `docker.io`,while in RedHat it is configured to pull images from `registry.access.redhat.com` .

#### Container Image

A container image, which is a file, pulled from the registry server and used locally as a mount point when the containers are started.LXD pulls a single container image from the registry server,while docker uses OCI-based images which can be made of multiple layers.

It is not a single file on a registry server, when the term container image they often refer to repository which is a bundle of multiple container image layers.

Container Image Format - The Open Container Initiative is based on Docker V2 image format,has successfully unified across all container engines to use Docker V2 image format.

#### Container Engine

A container engine pulls the container image from the registry server,accepts user's requests via command line options and runs the container. There are many container engines like CRI-O, Docker, LXD.Most container engines do not run containers, they rely OCI compliant runtime like runc.

Container Engine responsibilites:

  - Handling user input
  - Handling input over an API from the container orchestration.
  - Pulling the container image from registry server.
  - Container mount point, COW(Copy on Write) storage.
  - Preparing container metadata, which will be passed to container runtime.
     a. Using default container image.
  - Calling the container runtime.

#### Container Host 

A container host is the system that runs the containerized process(containers). For example a VM in a data center is a host, while containers can be run on them.

#### Container Runtime

Container Runtime is a low level of Container engine `runc` is the default container runtime defined by Open Container Initiative (OCI).

Container Runtime responsibilites:

  - Setting up cgroups.
  - Setting up AppArmor policies.
  - Setting up SELinux policies.
  - Communicating with linux kernel to start containerized process.
  - Container mount point.
  - Container metadata.

#### Image,Tag

Images: Repositories are often reffered to as images or container images,but they are actually made up of one or more image layers.Image Layers are usually referenced by parent child relationship.

Tag: Defining versions for the container images with the help of tags.

#### Container Orchestration

Teams started pulling container images from the registry server, and them modify or build new container images and push them to the registry server to share it with others on their team. Next step would be to bundle a few containers together and deploy them as unit. At some point of time they want to push the unit to a pipeline maybe Dev,QA,Prod, this path leads towards the realization that orchestration is needed.

A container orchestrator does two things: 
  - Dynamically schedules container workloads, within a cluster of computers.
  - Provides a standardized application definition file (kube file, docker compose).

Above two features provides many capabilites:

- Allows containers within an application to be scheduled completely separately utilizing system resources(Container Hosts), failure of container engines, IO error of containers, individual containers should be scaled up or scaled down.

- Easy to deploy new instances of the same application into new environments.In a cloud native world, there are some reasons to use container orchestration.

  a. QA environment running internally.
 
  b. Developer's laptop with container orchestration running.
  
  c. Load test environment dynamically.
 
  d. Production environment.
  
  e. Disaster Recovery Enviroment.
 
  d. New Production environment which has upgraded container hosts,container engines, or container orchestration.
 
  f. New production environment with the same versions of container hosts,container engines, or container orchestration in new geolocation(APAC,EMEA, etc).

  There are many container orchestration platforms available, `Swarm`, `Mesos`, `Kubernetes`.Kubernetes is defacto standard in container orchestration