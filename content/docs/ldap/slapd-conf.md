---
title: OpenLDAP (slapd.conf)
next: /docs/openldap/slapd-conf
prev: /docs/interception-vimproved
---

#### slapd - Standalone LDAP Daemon

```slapd``` is a LDAP directory server,which stands for Standalone LDAP daemon.Providing simple auth and security layer.

```bash
$ sudo apt install slapd ldapvi ldap-utils
```

{{< callout type="warning" >}}
 when asked for administration password prompt during installation just press ```Enter```,we reconfigure slapd using dpkg-reconfigure after the installation.
{{< /callout >}}

```bash
$sudo dpkg-reconfigure slapd
```
{{< callout type="info" >}}
Reconfiguration: 
1. Omit initial LDAP server config : ```No``` we obviously want to create intial configuration.  
2. DNS Domain Name : domain name to build the base DN of LDAP directory in this case we are choosing ```vinay.im```.  
3. Organization Name: Type down the organization name( here XYZ Pvt Ltd)
5. Choose an Admin Password of your choice( for tutorial purpose i've choosed test) and choose MDB as backend database
6. If asked to purge database when slapd is removed we choose ```No```,will be helpful when we want to switch to a different LDAP server. 
7. Choose Yes if you want to backup the current existing database to ```/var/backups```.

{{< /callout >}}

To have a look at the LDAP database , simple execute ```slapcat``` with sudo privileges.
```bash{filename="$ sudo slapcat"}
$ sudo slapcat
dn: dc=vinay,dc=im
objectClass: top
objectClass: dcObject
objectClass: organization
o: XYZ Pvt Ltd
dc: vinay
structuralObjectClass: organization
entryUUID: 8057316c-ed6e-103d-8b93-b9da23579469
creatorsName: cn=admin,dc=vinay,dc=im
createTimestamp: 20230922083350Z
modifiersName: cn=admin,dc=vinay,dc=im
modifyTimestamp: 20230922083350Z
```
{{< callout type="info" >}}
Config files are present in ```/etc/ldap``` directory.   
 Schemas can be added within the ```slap.d``` directory for server customization.  
 Database is stored in ```/var/lib/ldap``` having two files ```data.mdb``` and ```lock.mdb```.
{{< /callout >}}

```bash
$ sudo cp /usr/share/doc/slapd/examples/slapd.conf /etc/ldap/
```
Copy the example config file ```slapd.conf``` to ```/etc/ldap```, and replace DNS domain components ```dc=example``` to ```dc=vinay``` and ```dc=com``` to ```dc=im``` everywhere in the config, also
update ```/etc/default/slapd``` from  ```SLAPD_CONF``` to ```SLAPD_CONF=/etc/ldap/slapd.conf``` and update slapd service by ```sudo systemctl restart slapd``` 

In ```/etc/ldap/slapd.conf``` under ```suffix       "dc=vinay,dc=com"``` add the following lines 
```bash
rootdn          "cn=admin,dc=vinay,dc=com"
rootpw          "test"
```
Restart the slapd service again.
```bash
$ sudo systemctl restart slapd
```
#### ldapsearch

```bash{filename="ldapsearch anonymous query"}
$ldapsearch -x -b "dc=vinay,dc=im"
# vinay.im
dn: dc=vinay,dc=im
objectClass: top
objectClass: dcObject
objectClass: organization
o: XYZ Pvt Ltd
dc: vinay

# search result
search: 2
result: 0 Success

# numResponses: 2
```
```bash{filename="ldapsearch authenticating with admin user"}
$ ldapsearch -D cn=admin,dc=vinay,dc=im -w test -b dc=vinay,dc=im
# extended LDIF
#
# LDAPv3
# base <dc=vinay,dc=im> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# vinay.im
dn: dc=vinay,dc=im
objectClass: top
objectClass: dcObject
objectClass: organization
o: XYZ Pvt Ltd
dc: vinay

# search result
search: 2
result: 0 Success

# numResponses: 2
# numEntries: 1
```
1.  ```-D``` {dn} / ```--bindDN``` {dn} — The DN to use to bind to the directory server when performing simple authentication,to use the distinguished binddn name to bind the LDAP directory.
2. ```-w``` - this option is used to provide the password on the command line for auth, ```-W``` option is used to ask for prompt for typing invisible password without actualling having to type the pass on cli.
3. ```-b``` - search base as the starting point for the search instead of default.
4. ```-x``` option in ldapsearch is used for simple authentication instead of SASL.

The above command search's through the ldap directory server with ```admin``` distinguished name providing password with the ```-w``` option and setting the searchbase to start from the rootdn.

-- To list all users on ldap
```bash
$ ldapsearch -D "cn=admin,dc=vinay,dc=com" -W -b "dc=vinay,dc=com"
$ slapcat
```
lists all users from the base dn

#### Adding OU (Organization Unit)

Organizational units (OUs) are used to organize entries within the directory tree and can be used to delegate administrative responsibilities within your organization. It’s important to keep your directory organized and well-structured from the beginning; otherwise it will quickly become unwieldy and difficult to manage.

Create a directory called ldif(LDAP Interchange Format) in ```/etc/ldap``` and create a file called people.ldif and paste the following contents.
```bash
$ cat /etc/ldap/ldif/people.ldif
dn: ou=People,dc=vinay,dc=com
ou: People
cn: people
sn: people
objectClass: top
objectClass: inetOrgPerson

$ ldapadd  -D cn=admin,dc=vinay,dc=im -w test -f /etc/ldap/ldif/people.ldif 
adding new entry "ou=People,dc=vinay,dc=im"
```
now ```slapcat``` command shows the OU added within the command output. 

#### Add new User

Adding new user within the newly created OU(Organizational Unit)
 
 ```bash{filename="/etc/ldap/john.ldif"}
 # cat john.ldif 
dn: uid=john,ou=People,dc=vinay,dc=com
objectClass: top
objectClass: inetOrgPerson
uid: john
cn: john
ou: People
sn: abraham
mail: john@vinay.com
userPassword: john
```
Adding the .ldif file using ldapadd command
```
$ sudo ldapadd -D "cn=admin,dc=vinay,dc=com" -W -f john.ldif 
Enter LDAP Password: 
adding new entry "uid=john,ou=People,dc=vinay,dc=com"

```

#### Read entries within OU as admin

Now we have added an ```OU``` and a user ```john``` to ```People``` OU,lets try to ```ldapsearch``` the users within the OU as admin
```bash{filename="display users of an OU as admin"}
$ ldapsearch -D "cn=admin,dc=vinay,dc=com" -w vinay.com -b "ou=People,dc=vinay,dc=com"

# extended LDIF
#
# LDAPv3
# base <ou=People,dc=vinay,dc=com> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# People, vinay.com
dn: ou=People,dc=vinay,dc=com
ou: People
cn: people
sn: people
objectClass: top
objectClass: inetOrgPerson

# john, People, vinay.com
dn: uid=john,ou=People,dc=vinay,dc=com
objectClass: top
objectClass: inetOrgPerson
uid: john
cn: john
ou: People
ou: Support
sn: abraham
mail: john@vinay.com
userPassword:: am9obg==

```
#### Read entries within OU as normal user.

```bash
$ ldapsearch -D "uid=john,ou=People,dc=vinay,dc=com" -w john -b "ou=People,dc=vinay,dc=com"
# extended LDIF
#
# LDAPv3
# base <ou=People,dc=vinay,dc=com> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# People, vinay.com
dn: ou=People,dc=vinay,dc=com
ou: People
cn: people
sn: people
objectClass: top
objectClass: inetOrgPerson

# john, People, vinay.com
dn: uid=john,ou=People,dc=vinay,dc=com
objectClass: top
objectClass: inetOrgPerson
uid: john
cn: john
ou: People
ou: Support
sn: abraham
mail: john@vinay.com
userPassword:: am9obg==

```

#### Modifying existing entries

1. Using ```ldapmodify``` to update entries. 

Now to modify an already added record we use ldapmodify and the attributes that are to be modified are put into a separate file,here ```john-modify.ldif``` and to demonstrate here an OU ```Support```
is added to the existing entry,along with ```People``` OU.

```bash{filename="john-modify.ldif"}
$  cat /etc/ldap/ldif/john-modify.ldif 
dn: uid=john,ou=People,dc=vinay,dc=com
changetype: modify
add: ou
ou: Support
```

```bash{filename="ldapmodify command for john-modify.ldif"}\
$ ldapmodify -D "cn=admin,dc=vinay,dc=com" -W -f john-modify.ldif
Enter LDAP Password: 
modifying entry "uid=john,ou=People,dc=vinay,dc=com"
```

Now running a slapcat command shows the updated OU ```Support```

```bash{linenos=table}
dn: uid=john,ou=People,dc=vinay,dc=com
objectClass: top
objectClass: inetOrgPerson
uid: john
cn: john
ou: People
ou: Support
sn: abraham
mail: john@vinay.com
userPassword:: am9obg==
structuralObjectClass: inetOrgPerson
entryUUID: 50ea0ea8-f23d-103d-816b-4d9c39504958
creatorsName: cn=admin,dc=vinay,dc=com
createTimestamp: 20230928112421Z
entryCSN: 20230928120656.291224Z#000000#000#000000
modifiersName: cn=admin,dc=vinay,dc=com
modifyTimestamp: 20230928120656Z
```

2.Using ```ldapvi``` to update LDAP entries with a text editor.

```bash{filename="ldapvi example"}
$  ldapvi -d --host vinay.im 
```
```ldapvi```  is a ldap client using which we can search,modify and delete entries which is easier than ```ldapmodify```  instead of adding the updated records in a separate ```ldif``` file.
ldapvi prompts to open text editor to modify entries,just similar to text editor.

The above command will bind anonmously to hostname, here the hostname is ```vinay.im```.After making necessary changes in the entry save from the text editor.
```
# ldapvi -d --host nextcloud.vinay.com
      3 entries read                                                                                                                                                                        
add: 0, rename: 0, modify: 1, delete: 0
Action? [yYqQvVebB*rsf+?] b


--- Login

--- Login

--- Login
Type M-h for help on key bindings.

Filter or DN: cn=admin,dc=vinay,dc=im

    Password: *****

Bound as cn=admin,dc=vinay,dc=im.
add: 0, rename: 0, modify: 1, delete: 0
Action? [yYqQvVebB*rsf+?] y
Done.
```
after saving and exiting from text editor, an interactive bash prompt ``` [yYqQvVebB*rsf+?]``` 

```y``` to commit changes.

```e``` to edit changes.

```v``` to view changes as LDIF change records.

```b``` to show login and rebind - we are trying to auth from admin and save the changes to LDAP entries.


```
[Reference serverfault] https://serverfault.com/questions/290296/ldapadd-ldapmodify-clarifications-needed-about-these-commands

#### Verifying the ```slapd.conf``` Configuration file

```bash
$sudo  slaptest -v -f /etc/ldap/slapd.conf 
config file testing succeeded
```

```-f``` : Specifying an alternative configuration file.

```-v``` : enable verbose mode.



#### Conventions in OpenLDAP

dn - Distinguished Name  
RDN - Relative Distinguished Name  
cn - Common Name  
dc - Domain Component  
mail - Email Address  
ou - Organization Unit  
ldif - LDAP Data Interchange Format  
ldap - Lightweight Directory Access Protocol  

### References

1. https://access.redhat.com/documentation/en-us/red_hat_directory_server/11/html/deployment_guide/introduction_to_directory_services

2. https://www.zytrax.com/books/ldap

3. https://tylersguides.com/guides/openldap-how-to-add-a-user/

4. https://www.zytrax.com/books/ldap/
