---
title: Terminology
next: /docs/openldap/slapd-conf
prev: /docs/interception-vimproved
---

### Terminology used in LDAP
{{< filetree/container >}}
  {{< filetree/folder name="DIT Directory Information Tree" state="closed">}}
             {{< filetree/folder name="root entry {Object}(Also called Objects)" >}}
                    {{< filetree/folder name="objectclass 1" state="closed" >}}
                        {{< filetree/file name="This object class consists of zero or more attributes" >}}
                    {{< /filetree/folder >}}
                    {{< filetree/folder name="objectclass 2" state="closed">}}
                        {{< filetree/file name="This object class consists of zero or more attributes" >}}
                    {{< /filetree/folder >}}
             {{< /filetree/folder >}}
    {{< filetree/folder name="entry {Object} " state="closed" >}}
      {{< filetree/file name="ObjectClass" >}}
      {{< filetree/file name="ObjectClass" >}}
    {{< /filetree/folder >}}
  {{< /filetree/folder >}}
{{< /filetree/container >}}



1. Data is represented as hierarchy of objects,each of which is called entry.The resulting tree structure is called a Directory Information Tree (DIT).
The top entry of the tree is called root.

2. Each entry in the tree has one parent entry and zero or more child entries.

3. Each entry is composed of one or more objectClasses. -> These objectClasses contain zero or more attributes.

4. These attributes have names just like values.

### ObjectClasses.

Objectclasses is like a container of attributes,where each objectClass has a unique name.There exists a predefined set of ObjectClasses,each of which contains lot of attributes.

Characteristics of ObjectClasses:

1. objectClasses defines whether the attribute member should be ```MUST``` or ```MAY``` be present.
2. objectClass types: ```STRUCTURAL```,```AUXILIARY```,```ABSTRACT```, and there should be atleast one ```STRUCTURAL``` objectClass with zero or more AUXILIARY objectClasses.
3. objectClass inherits properties and characteristics from its parent objectClass (including its attributes). 


### Attributes.

Attributes contains values,which are present within the objectClass,each attribute define the data type.
Attributes are mostly defined in ```key=value``` pair.

1. Attributes can be optional ```MAY``` or mandatory ```MUST``` defined in that object class.If it is inherited by multiple objectClasses, in one objectClass it can be mandatory
in another object class it can be optional, this is defined by the objectClass.

2. Attributes can be ```SINGLE``` or ```MUTLI ``` valued.

some examples for attributes like ```cn``` aliased ```commonName``` which is within the objectClass ```person```,```organizationalPerson``` and so on..

Another example is ```dc``` aliased ```domainComponent``` which is present under the objectClass ```dcObject```.

```bash
# Entry Level Hierarchy
dn: dc=example,dc=com
dc: example
description: The best company in the whole world
objectClass: dcObject
objectClass: organization
o: Example, Inc.

## FIRST Level hierarchy - people 
# this is an ENTRY sequence and is preceded by a BLANK line

dn: ou=people, dc=example,dc=com
ou: people
description: All people in organisation
objectClass: organizationalUnit

## SECOND Level hierarchy - people entries 
# this is an ENTRY sequence and is preceded by a BLANK line

dn: cn=Robert Smith,ou=people,dc=example,dc=com
objectclass: inetOrgPerson
cn: Robert Smith
cn: Robert
sn: Smith
uid: rsmith
mail: robert@example.com
mail: r.smith@example.com
ou: sales

```

DN attribute is the sum of all the RDN (Root Distinguished Name)

