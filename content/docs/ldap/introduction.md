---
title: Introduction 
next: /docs/openldap/slapd-conf
prev: /docs/interception-vimproved
---

### Directory Service

{{< callout type="info" >}}
[Difference between a folder and a directory](https://stackoverflow.com/questions/5078676/what-is-the-difference-between-a-directory-and-a-folder)
Folder is for grouping items.
Directory has index. It is for finding specific item,Directory is a filesystem concept.
In simple terms think ```directory``` like a telephone directory which is in a hierarchial structure.
{{< /callout >}}

The term directory service refers to the collection of software, hardware, and processes that store information about an enterprise, subscribers, or both, and make that information available to users. A directory service consists of at least one instance of Directory Server and at least one directory client program. Client programs can access names, phone numbers, addresses, and other data stored in the directory service.

A directory is similar to database,which is attribute-based data;where data is read more often than write.

Directory Server provides Global Directory Services which means it provides information to wide variety of applications,rather than using databases with different applications,which is very hard to administrate.Directory server is a single solution to manage the same information
{{<callout type="info">}}
For example, an organization has three different applications running like nextcloud,email and matrix server and all the applications are accessed by same credentials,if separate database schema's are used for each application it would be hard to manage,if user requesting a password change in one application maybe not be replicated into another application;this problem is solved single,centralized repository of directory information.
{{</callout>}}

LDAP provides a common language that client applications and servers use to communicate with one another. LDAP is a "lightweight" version of the Directory Access Protocol (DAP)

### LDAP vs Database

{{% details title="1. How often does your data change?"%}}
Directory servers are used for ```reads```,if your data changes often and have many write operations directory service is not a ideal choice,RDBMS would be the ideal choice.
{{% /details %}}


{{% details title="2. Type of Data? "%}}
If data is defined in ```Key:Value``` pair or ```Attribute:Value``` pair, Directory service would be the best choice,like user profile.
{{% /details %}}

{{% details title="3. Data in Hierarchial tree like structure" %}}
If data can be modeled into a tree like structure,accessing the parent and child node in the tree,directory service 
{{% /details %}}

The ```read:write``` ratio in LDAP is of ```500:1``` so more number of read operations are done than the write/update operations.
### OpenLDAP
LDAP stands for Lightweight Directory Access Protocol, for accessing directory services.OpenLDAP is the implementation of the LDAP protocol,is a communications protocol that defines the methods in which a directory service can be accessed. 
The LDAP information model is based on entries, which is a collection of attributes that has a globally unique Distinguished Name```(DN)```
OpenLDAP is the implementation of the LDAP protocol which belong to User Management and Authentication in tech.

The LDAP protocol both authenticates and authorize's users to their resources.The protocol authenticates users with a bind operation that allows users to communicate with LDAP directory
then authorizes the authenticated user to resources they need if they have access that are defined in rules.Once a user is successfully authenticated, they need to be authorized to access the resource(s) requested. 
With OpenLDAP, for example, users belong to groups that can be assigned different permissions. If the authenticating user is assigned the correct permissions to access a certain resource, the LDAP protocol will authorize them to it; if not, the protocol will deny access.
#### LDAP Data components

1. Directory: an LDAP server

2. DIT: the tree of entries stored within a directory server

3. Attributes 

Data in LDAP system is stored in elements called attributes,like Key Value pair.Data in the attribute must match to the type defined in the attribute's initial declaration.

```bash
mail: user@example.com
dc:example,dc:com
```

4. Entries 

Attributes by themselves are not useful, a group or collection of ```attributes``` under a name represents an entry.
```bash
dn: ou=people,dc=example,dc=com
objectClass: person
sn: Ramesh
cn: Varma
```
An example entry displayed in LDIF ( LDAP Data Interchange Format).
```bash
$ cat ldif/user.ldif 
dn: uid=vinay.m,ou=People,dc=vinay,dc=im
objectClass: top
objectClass: inetOrgPerson
uid: vinay.m
cn: vinay
sn: m
userPassword: test
ou: People

dn: uid=akshay,ou=People,dc=vinay,dc=im
objectClass: top
objectClass: inetOrgPerson
uid: akshay
cn: akshay 
sn: p
userPassword: test
ou: People
```

5. ObjectClass

Object class: a collection of required (MUST) and optional (MAY) attributes. Object classes are further subdivided into STRUCTURAL and AUXILIARY classes, and they may inherit from each other.Every entry has a structural Object class which indicates what type of object an entry is and also can have more auxiliary object that have additional characteristics for that entry.

 The ObjectClass definitions are stored in the schema files.Object class must have an object identifier (OID)  Object classes may also list a set of required attribute types (so that any entry with that object class must also include those attributes) and/or a set of optional attribute types (so that any entry with that object class may optionally include those attributes).OID's are sequence of numbers separated by periods(.), “1.2.840.113556.1.4.473” 

6. Schema 

  Schema's define the directory, specifying the configuration of the directories including syntax,object classes,attribute types and matching rules.

### References

1. https://access.redhat.com/documentation/en-us/red_hat_directory_server/11/html/deployment_guide/introduction_to_directory_services

2. https://www.zytrax.com/books/ldap

3. https://tylersguides.com/guides/openldap-how-to-add-a-user/

4. https://www.zytrax.com/books/ldap/
