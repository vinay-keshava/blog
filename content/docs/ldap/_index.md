---
title: LDAP
type: docs
next: /docs/selfhosting
sidebar:
  open: false
---

Documenting stuff learning about OpenLDAP with two types of configuration, the traditional slapd.conf and slapd.d way of configuring.
