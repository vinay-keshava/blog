---
title: GnuPG 101
authors:
  - name: Vinay Keshava
    link: https://winay.in/about
---

#### Import GPG private key to another computer

`gpg --import private.asc` is the command to import the GnuPG private key onto another computer.

```bash
vinay@goodachari:~/Documents$ gpg --import private.asc 
gpg: directory '/home/vinay/.gnupg' created
gpg: keybox '/home/vinay/.gnupg/pubring.kbx' created
gpg: /home/vinay/.gnupg/trustdb.gpg: trustdb created
gpg: key D3FDE3D93A1FCF50: public key "Vinay Keshava <vinaykeshava@disroot.org>" imported
gpg: key D3FDE3D93A1FCF50: secret key imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg:       secret keys read: 1
gpg:   secret keys imported: 1
```

Now edit the imported key and trust the key `ultimate`ly.
```bash
vinay@goodachari:~/Documents$ gpg --edit-key D3FDE3D93A1FCF50 trust quit
gpg (GnuPG) 2.2.40; Copyright (C) 2022 g10 Code GmbH
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

ssb  cv25519/FD6F2858DED47C32
     created: 2022-11-25  expires: never       usage: E   
[ unknown] (1). Vinay Keshava <vinaykeshava@disroot.org>

sec  ed25519/D3FDE3D93A1FCF50
     created: 2022-11-25  expires: never       usage: SC  
     trust: unknown       validity: unknown
ssb  cv25519/FD6F2858DED47C32
     created: 2022-11-25  expires: never       usage: E   
[ unknown] (1). Vinay Keshava <vinaykeshava@disroot.org>

Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  1 = I don't know or won't say
  2 = I do NOT trust
  3 = I trust marginally
  4 = I trust fully
  5 = I trust ultimately
  m = back to the main menu

Your decision? 5
Do you really want to set this key to ultimate trust? (y/N) y

sec  ed25519/D3FDE3D93A1FCF50
     created: 2022-11-25  expires: never       usage: SC  
     trust: ultimate      validity: unknown
ssb  cv25519/FD6F2858DED47C32
     created: 2022-11-25  expires: never       usage: E   
[ unknown] (1). Vinay Keshava <vinaykeshava@disroot.org>
Please note that the shown key validity is not necessarily correct
unless you restart the program.

vinay@goodachari:~/Documents$ gpg --list-keys
gpg: checking the trustdb
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
/home/vinay/.gnupg/pubring.kbx
------------------------------
pub   ed25519 2022-11-25 [SC]
      2F7E32FC9C01622BC797E60AD3FDE3D93A1FCF50
uid           [ultimate] Vinay Keshava <vinaykeshava@disroot.org>
sub   cv25519 2022-11-25 [E]
```
And upon the listing the GnuPG keys present, the imported key with new level ultimate trust is displayed.