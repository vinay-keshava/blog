---
title: EPM (ESP Package Manager)
authors:
  - name: Vinay Keshava
    link: https://blog.vinay.im/about
next: /docs/hacks/
---

[EPM](https://github.com/jimjag/epm) is a ESP Package Manager used to package software easily,either by creating debs or rpms or tar.gz which makes easy to distribute software.
Software distribution is the most difficult task in today's world, and packaging software for each distribution makes it tedious,every distribution has their own set of rules and 
development tools to package software.

EPM makes it easy to ship and package software for every distribtion.

To get started with EPM here is the documentation [link](https://github.com/jimjag/epm/blob/trunk/doc/epm-book.pdf).

You can install epm on Debian by just 
 ```bash
  $ sudo apt install epm
 ``` 
 Or can build from source too.

### Packaging using EPM

An example demo to package prometheus to generate a deb and tar.gz using EPM.

Step 1: Download the tarball prometheus-2.48.0.linux-amd64.tar.gz

Step2: Extract to the new folder 

Step 3: Create a new file in the extracted folder called prometheus.list with the following contents and here is the FHS to package

 ```bash{filename="ls -l prometheus-2.48.0.linux-amd64"}
 $ ls -l prometheus-2.48.0.linux-amd64
total 171576
-rw-r--r-- 1 debian debian    11357 Nov 16 05:01 LICENSE
-rw-r--r-- 1 debian debian     3773 Nov 16 05:01 NOTICE
drwxr-xr-x 2 debian debian     4096 Nov 16 05:01 console_libraries
drwxr-xr-x 2 debian debian     4096 Nov 16 05:01 consoles
drwxr-xr-x 2 root   root       4096 Nov 28 06:30 linux-6.1-x86_64
-rwxr-xr-x 1 debian debian 90226488 Nov 28 06:29 prometheus
-rw-r--r-- 1 debian debian      424 Nov 27 10:19 prometheus-systemd-service
-rw-r--r-- 1 debian debian      734 Nov 28 06:28 prometheus.list
-rw-r--r-- 1 debian debian      934 Nov 16 05:01 prometheus.yml
-rwxr-xr-x 1 debian debian 85422008 Nov 28 06:29 promtool
 ```

```bash{filename=prometheus.list}
%product Prometheus
%copyright Promethues[Expat], All Rights Reserved.
%vendor 
%license LICENSE
%description Prometheus  
%version 2.48.0
%literal(control) <<EOF
Section: misc
Priority: important
EOF
f 755 root sys /usr/local/bin/prometheus prometheus
f 755 root sys /usr/local/bin/promtool promtool
d 755 root sys /etc/prometheus/consoles consoles
d 755 root sys /etc/prometheus/console_libraries console_libraries
f 644 root sys /etc/prometheus/prometheus.yml prometheus.yml 
f 644 root sys /lib/systemd/system/prometheus.service prometheus-systemd-service
%postinstall sudo systemctl daemon-reload
%postinstall sudo systemctl enable prometheus
%postinstall echo Installation completed prometheus running on port 9090
```

### Terminologies

1. ```%product``` describes the name of the package.

2. ```%copyright``` describes the license of the package.

3. ```%vendor ``` describes the organization name.

4. ```%license ``` attaches the file called LICENSE of the prometheus package. 

5. ```%description``` description about the package.

6. ```%literal ``` literal data, debian packages have control file to provide metadata about each package,similarly this token can be used to produce similar metadata.
 
7. ```%postinstall``` &   ```%preinstall``` to execute any scripts after and before installing the package respectively.

8. Here,comes the actual part files,directories and symlinks

  To put files/directories in FHS follow the below example syntax 

 ```type mode owner group destination               source      options   ```

 ```f     755 root   sys  /usr/local/bin/prometheus prometheus  ``` 
 
 Here  
 -> ```type ``` describes whether its a file(f), or directory(d) or symbolic link (l) or configuration file(c)
  
 ->  ```mode ``` describes about the file permission in unix style also mentioning the user and the group level access 

 -> ```destination``` &  ```source``` specifies which file or directory to be put,here  ```/usr/local/bin/prometheus```  is the destination and the binary  ```prometheus ```
  in the current directory is the source



Below is the prometheus systemd service file example to enable systemd for prometheus and hence we use   ```%postinstall``` to reload the systemd daemon and setup the systemd service.

```bash{filename="prometheus systemd service file"}
 $ cat prometheus-systemd-service 
[Unit]
Description=Prometheus Time Series Collection and Processing Server
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/usr/local/bin/prometheus    --config.file /etc/prometheus/prometheus.yml --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
```

### Building the package

```bash
$ sudo epm -f deb prometheus
```

Will look for prometheus.list we had configured above and tries to build the package.

The   ```-f ``` option is to specify the distribution format here, giving it as  ```deb ``` to build a deb.

If the command runs successfully it creates and directory within the current directory called  ```linux-6.1-x86_64 ``` the built package deb is present within this directory.

Running without the  ```-f ``` option builds a tarball  as show below.



```bash{filename="within the extraced directory"}
$ sudo epm prometheus
$ ls linux-6.1-x86_64/
prometheus-2.48.0-linux-6.1-x86_64.tar.gz

$ sudo epm -f deb prometheus
$ ls linux-6.1-x86_64/
prometheus-2.48.0-linux-6.1-x86_64.deb  prometheus-2.48.0-linux-6.1-x86_64.tar.gz
```

### Drawbacks

1. EPM does not support alphanumeric characters and special characters like underscore for the package name making it difficult and reducing reading enhancibility.
