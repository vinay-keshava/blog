---
title: OpenShift 101
authors:
  - name: Vinay Keshava
    link: https://winay.in/about
---

Redhat OpenShift is a platform that allows us to manage,deploy and orchestrate containerized applications in the cloud.Red hat openshift solves the biggest problem of developing and deploying microservice based applications.

Redhat Openshift makes it easy for Development and Operations

Dev - Code,Libraries,Configuration,Server Runtime,OS

Ops - Logging,Remote access,Network configuration,monitoring

For Developers Team, it makes easy to focus on what developers are best at doing, developing, commiting code.For Ops team managing logs, monitoring applications and network configurations.

OpenShift includes a fully compliant instance of upstream Kubernetes and is tested with dozens of technologies.Redhat OpenShift ships with Redhat Enterprise Linux, CoreOS and supports RedHat Enterprise Linux for worker nodes and standard Docker and [CRI-O](https://cri-o.io/) runtimes for containers managed by Kubernetes.

#### Getting started with Openshift CLI 

OpenShift is often referred to as a container application platform in that it's a platform designed for the development and deployment of applications in Linux containers.

`oc` command lets you work with the entire OpenShift cluster and deploy new applications.The CLI exposes the underlying Kubernetes orchestration system with the enhancements made by OpenShift. Users familiar with Kubernetes will be able to adapt to OpenShift quickly.

Logging In with the CLI

`oc login -u admin -p admin https://api.crc.testing:6443 --insecure-skip-tls-verify=true`

the command to login into openshift for the user (`-u` option) `admin` with password (`-p`option above)  `admin` 

`oc whoami` : displays the logged in openshift user.

 - Create a project on the OC webconsole by clicking on `+ADD`,let us create a sample project with the name `myproject`

 - Import a git url project, provide the link to the spring boot project git repository 

