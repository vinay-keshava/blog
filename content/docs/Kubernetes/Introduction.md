---
type: docs
title: "Introduction to Kubernetes"
draft: false
---

Kubernetes is a container orchestration platform for managing workloads and services. Kubernetes has a rapidly growing ecosystem.
Often abbreviated as `k8s` which counts 8 between `k` and `s` in Kubernetes.

### Monolith to Containers

Let us divide the evolution of deploying application into three types:

1. Traditional deployment Era
2. Virtualized deployment Era
3. Container deployment Era

`Traditional deployment`, applications were deployed directly on the physical server stack, and if the resources has to be scaled up, the server specification were increased.If multiple applications are deployed on the same server stack one application would endup taking much resources, and other applications would underperform.

`Virtualized Deployment`, after the traditional server stack deployment, then arises Virtualized Deployment with the help of Virtual Machine,allowing to run multiple VM's (Virtual Machines) on a single server with the help of a virtualization hypervisor. Each VM running different applications thereby making it isolated with different operating system for each virtual machine.

`Container Deployment`, containers are similar to VM's with minimal requirements to run the application and to bootup.
Like VM's the container has a separate filesystem,share of CPU, memory, process space.

Containers are popular because:

- Agile methodology
- Continous development,integration and development.
- Portability
- Resource isolation

Containers bundles the application and its dependencies to run the application within a minimalistic container to start and run the application. To manage multiple containers(to cope-up with the load) and on failure of the containers to restart them,if one container goes down another container has to be started automatically, here comes Kubernetes.Kubernetes takes care of scaling, restarting the failed containers, flushing unused containers, 

Kubernetes provides the following features: 

- Service discovery and load balancing
- Storage orchestration
- Automated rolling updates and rollbacks
- Automatic bin packaging
- Self healing
- Secret and configuration management
- Batch execution 
- Horizontal scaling
- IPV4 / IPV6 dual stack 

### Kubernetes Components


Architecture: 
![](https://kubernetes.io/images/docs/components-of-kubernetes.svg)

Source: kubernetes.io

Kubernetes architecture consists of various components that help manage clusters. A kubernetes cluster is a group of machines called nodes, that are used to run containerized applications.These nodes are managed by k8s for scaling,monitoring, deployment etc.

`Nodes`: nodes are the worker nodes, which is physical server or VM's. Pods are deployed on the node and managed by the control plane.

`Pods`: basic unit of deployment in Kubernetes, a pod is a group of one or more containers that are deployed on the same node. Pods host the application

Date/Node Components: Node components run on every node for maintainance of running pods.

1. `kubelet`: make sure containers are running in a pod. Also responsible for registering a node within cluster.Communicates with API server to alert downtime issues and auto healing.

2. `container runtime`: responsible for executing applications within container. container runtimes are docker, podman, containerd, cri-o

3. `kubeproxy`: is a network proxy that runs on each node, maintains network rules on nodes. These rules defined allow network communication to pods from network.


Control Plane Components:

Control plane is the brain of the kubernetes platform, monitoring every operations of the the cluster.Control Plane manages cluster events, like starting up new pod when more number of replicas are needed.

1. `Kube-API server`: acts as gateway, exposes the cluster to external traffic, the entry point of Kubernetes API.Heart of the kubernetes cluster as it centrally manages and directs the worker nodes to handle requests based on traffic analysis.

2. `Kube- Scheduler`: executes the decisions made by kube API server, translating the directives into actionable tasks within the cluster.

3. `etcd`: to store cluster information like configuration data, state data, meta data. `etcd` is like a key-value store distributed across systems.

4. `kube-controller-manager`: runs controller process.
- Node Controller: responsible for monitoring node's downtime.
- Job Controller: creates pods to run tasks for its completion.
- Endpoint slice Controller: link between services and pods.
- Service Account Controller: service account for new namespaces.

5. `cloud-controller-manager`: responsible for communication between the kubernetes cluster and the cloud provider(AWS,GCP,AZURE).For example a request from the cloud provider(say AWS) to check whether the nodes were deployed, this request is passed on from the cloud provider API (here AWS) to Cloud controller manager, the cloud controller manager passes onto kube-api-server to give instructions and execute the task.



#### Addons

- `DNS`: every k8s cluster has DNS, cluster DNS is a DNS server which serves DNS records for k8s services.
- `Web UI`: Web UI to manage and troubleshoot applications running in the cluster. 
- `Container Resource Monitoring`: records a generic time-series metrics about containers.
- `Logging`: Cluster level logging for saving logs to central log store of containers.
- `Network Plugins`: assigning IP addresses to pods and enabling them to commmunicate within the cluster.

`k3s` is a lightweight distribution of `k8s`
