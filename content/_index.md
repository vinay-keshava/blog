---
title: Dreams drives deeds,words!
toc: false
---

<img src="/images/vinay.png" alt="vinay" style="width:150px;"/>

Hello 👋, I'm Vinay.


## Recent Posts
{{< cards >}}
    {{< card link="/blog/lap-23/" title="Lap'23" icon="arrow-circle-right"  tag= "new" tagColor="yellow" >}}
    {{< card link="/blog/convocation/" title="Convocation" icon="arrow-circle-right"  >}}
    {{< card link="/blog/kumara-parvatha/" title="Kumara Parvatha" icon="arrow-circle-right"  >}}
    {{< card link="/blog/new-domains/" title="New Domains" icon="arrow-circle-right"  >}}
     {{< card link="/blog/debconf23/" title="DebConf-23!! " icon="arrow-circle-right"  >}}

{{< /cards >}}
## Explore

{{< cards >}}
  {{< card link="about" title="About" icon="user" >}}
  {{< card link="blog/" title="Blog" icon="document-text" >}}
  {{< card link="/captures" title="Captures" icon="camera" >}}
{{< /cards >}}



{{< callout type="info" >}}
Except where otherwise noted, content on this site is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International license.](http://creativecommons.org/licenses/by-sa/4.0/)
{{< /callout >}}

