#!/bin/bash
hugo
tag=$(git describe --tags --abbrev=0)
message=$(git log $tag..HEAD --oneline --pretty=format:"%s")
git tag $(date +"%d.%m.%Y")
echo $message
echo $tag

# deploy to github pages
git clone git@github.com:vinay-keshava/vinay-keshava.github.io.git
cp -r public/* vinay-keshava.github.io && cd vinay-keshava.github.io
git add .
git commit -m "$message" -m "Tag: $tag"
git push --all
cd .. && rm -rf vinay-keshava.github.io

# deploy to gitlab pages
git clone git@gitlab.com:vinay-keshava/vinay-keshava.gitlab.io.git
cp -r public/* vinay-keshava.gitlab.io && cd vinay-keshava.gitlab.io
git add .
git commit -m "$message" -m "Tag: $tag"
git push --all
cd .. && rm -rf vinay-keshava.gitlab.io

# deploy to server
rsync -rP public/ root@65.21.189.158:/var/www/html/winay
